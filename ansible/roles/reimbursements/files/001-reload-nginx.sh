#!/bin/sh
set -euf
if [ -e /lib/systemd/system/nginx.service ]; then
	systemctl reload nginx.service
fi
