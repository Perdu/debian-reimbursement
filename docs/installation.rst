=========================
Installation instructions
=========================

#. Install dependency wkhtmltopdf and poppler-utils (for pdfunite)
#. Clone the git repository
#. Create a virtualenv to run it with::

    $ python3 -m venv ve

#. Install the dependencies in the virtualenv::

    $ ve/bin/python -m pip install -r requirements.txt

#. Create a ``localsettings.py``::

    $ cp src/debian_reimbursements/localsettings.py.sample \
         src/debian_reimbursements/localsettings.py

#. Configure critical settings in ``localsettings.py``:

   #. Enable ``DEBUG`` if doing local development.
   #. Your database (``DATABASES``).
   #. Your vhost name (``ALLOWED_HOSTS``).
   #. Your site identity (``SITE_NAME``, ``SITE_URL``).
   #. Generate a new random string for a secret key (``SECRET_KEY``).
   #. Outbound email (``EMAIL_HOST`, `DEFAULT_FROM_EMAIL``).
   #. Receipts storage (``RECEIPTS_PATH``).
      Make sure this is writeable by the web-server.
   #. Workflow defaults (``DEFAULT_CURRENCY``, ``DEFAULT_PAYER``, ``DEFAULT_APPROVER``).

#. Create the database::

    $ ve/bin/python src/manage.py migrate

#. Create the standard groups::

    $ ve/bin/python src/manage.py loaddata fixtures/groups.json

#. Create the standard expense types::

    $ ve/bin/python src/manage.py loaddata fixtures/standard_types.json

#. Import a set of historical exchange rates::

    $ ve/bin/python src/manage.py import_ecb_exchangerates --historical

#. Check the installation::

    $ ve/bin/python src/manage.py check --deploy

#. Create an initial superuser::

    $ ve/bin/python src/manage.py createsuperuser

#. In development: Run the development web server::

    $ ve/bin/python src/manage.py runserver

#. In production: Setup automatic exchange rate updates with systemd, using
   ``reimbursements-update-ecb-exchange.timer`` or
   ``reimbursements-update-oxr-exchange.timer``:

   #. Copy the appropriate ``.timer`` and ``.service`` file from
      ``docs/examples`` to ``/etc/systemd/system/``.
   #. Edit the paths in the ``.service`` file to point to your
      installation. Select the correct user to run the update.
   #. Tell systemd to read the files::

       # systemctl daemon-reload

   #. Enable the sytemd unit::

       # systemctl enable reimbursements-update-OPTION-exchange.timer

   #. Do the initial run::

       # systemctl start reimbursements-update-OPTION-exchange.timer

   #. Verify that it ran correctly::

       # journalctl log --unit=reimbursements-update-OPTION-exchange.service --since='1hr ago'

Open Exchange Rates
-------------------

The ECB exchange rate database only has a limited set of currencies in
it. `Open exchange rates <https://openexchangerates.org>`_ offers more
data for free, but requires an API key.

#. Sign up and generate an app ID.
#. Configure it in ``localsettings.py`` as ``OPEN_EXCHANGE_RATES_APP_ID``.
#. Import exchange rates::

    $ ve/bin/python src/manage.py import_openexchangerates --since=2023-01-01
