from django.contrib.auth.models import AbstractUser

from org_configuration.debian.models import NMStatus


def debian_status(user: AbstractUser) -> dict[str, str]:
    """Given a user, determine their Debian Contributor Status"""
    try:
        nm_status = NMStatus.objects.get(email=user.email.lower())
    except NMStatus.DoesNotExist:
        return {
            "bootstrap_color": "danger",
            "status": "No known Debian contributor status",
        }

    bootstrap_color = {
        "dc": "secondary",
        "dm": "info",
        "dd": "success",
    }[nm_status.status[:2]]

    return {
        "bootstrap_color": bootstrap_color,
        "status": NMStatus.NMStatuses[nm_status.status].label,
    }
