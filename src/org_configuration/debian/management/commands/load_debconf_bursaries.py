from csv import DictReader, DictWriter
from datetime import datetime
from decimal import Decimal

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.auth.models import User as UserType
from django.core.management.base import BaseCommand
from django.utils import timezone

from historical_currencies.exchange import exchange
from historical_currencies.formatting import render_amount

from reimbursements.models import (
    ExpenseType,
    Request,
    RequestHistory,
    RequestLine,
    RequestType,
)

User = get_user_model()

# Heuristic: What currency is likely the best option
# The requester can always change it later
COUNTRY_CURRENCY = {
    "AG": "EUR",
    "AL": "EUR",
    "AR": "USD",
    "AT": "EUR",
    "BE": "EUR",
    "BR": "BRL",
    "CA": "CAD",
    "CH": "CHF",
    "CN": "CNY",
    "CZ": "CZK",
    "DE": "EUR",
    "ES": "EUR",
    "FR": "EUR",
    "GB": "GBP",
    "GR": "EUR",
    "GT": "USD",
    "IE": "EUR",
    "IL": "ILS",
    "IN": "INR",
    "IT": "EUR",
    "JP": "JPY",
    "KR": "KRW",
    "MX": "MXN",
    "NG": "USD",
    "NP": "NPR",
    "PL": "PLN",
    "SE": "SEK",
    "TR": "USD",
    "TW": "USD",
    "US": "USD",
    "UY": "USD",
    "XK": "EUR",
    "ZA": "ZAR",
    "__": "USD",
}

CURRENCY_PAYER = {
    "BRL": "SPI",
    "CAD": "SPI",
    "CHF": "Debian.ch",
    "CNY": "SPI",
    "CZK": "Debian France",
    "EUR": "Debian France",
    "GBP": "Debian France",
    "ILS": "SPI",
    "INR": "SPI",
    "JPY": "SPI",
    "KRW": "SPI",
    "MXN": "SPI",
    "NPR": "SPI",
    "PLN": "Debian France",
    "SEK": "Debian France",
    "USD": "SPI",
    "ZAR": "SPI",
}


def existing_parent(arg: str) -> tuple[str, Request]:
    """Parse a --existing-parent argument"""
    budget_name, req_num = arg.split(":")
    parent = Request.objects.get(id=int(req_num))
    return (budget_name, parent)


def expense_type(arg: str) -> ExpenseType:
    """Look up an Expense Type by name"""
    try:
        return ExpenseType.objects.get(name=arg)
    except ExpenseType.DoesNotExist as e:
        raise ValueError(e)


def group_name(arg: str) -> Group:
    """Look up a Group by name"""
    try:
        return Group.objects.get_by_natural_key(arg)
    except Group.DoesNotExist as e:
        raise ValueError(e)


def pretty_budget_name(name: str) -> str:
    """Human readable budget name"""
    if name.startswith("dc-"):
        name = name.split("-", 1)[1]
        if name == "ctte":
            name = "DebConf Committee"
        else:
            name = f"DebConf {name.title()} Team"
    else:
        name = name.title()
    return name


class Command(BaseCommand):
    help = "Load a CSV file with a set of approved bursaries"

    def add_arguments(self, parser):
        parser.add_argument(
            "bursaries",
            type=open,
            metavar="CSV",
            help="List of bursaries to import into the system",
        )
        parser.add_argument(
            "--dry-run",
            action="store_true",
            help="Only print what we would do, don't perform any actions",
        )
        parser.add_argument(
            "--owner",
            metavar="USER",
            required=True,
            help="Create parent requests belonging to USER",
        )
        parser.add_argument(
            "--existing-parent",
            "-p",
            action="append",
            metavar="BUDGET:NUM",
            type=existing_parent,
            default=[],
            help="Attach requests for BUDGET to request NUM "
            "(can be specifide multiple times)",
        )
        parser.add_argument(
            "--approver-group",
            metavar="NAME",
            default="DebConf Treasurers",
            type=group_name,
            help="Make NAME the approvers for the created requests "
            "(default: DebConf Treasurers)",
        )
        parser.add_argument(
            "--payer-group",
            metavar="NAME",
            default="SPI",
            type=group_name,
            help="Make NAME the payers for the created requests " "(default: SPI)",
        )
        parser.add_argument(
            "--expense-type",
            metavar="NAME",
            default="Flight Tickets",
            type=expense_type,
            help="Create Request lines with NAME Expense Type",
        )
        parser.add_argument(
            "--event-name",
            metavar="NAME",
            default="DebConf",
            help="Name of the event",
        )
        parser.add_argument(
            "--expire-days",
            metavar="DAYS",
            default=60,
            help="Days to complete request transfer",
        )
        parser.add_argument(
            "--creation-date",
            metavar="TIMESTAMP",
            type=datetime.fromisoformat,
            default=timezone.now(),
            help=(
                "Date to backdate the request to. The bursaries deadline will "
                "give the best exchange rates."
            ),
        )

    def handle(self, *args, **options):
        self.budgets = dict(options["existing_parent"])

        r = DictReader(options["bursaries"])
        w = DictWriter(
            self.stdout,
            [
                "user.username",
                "converted_amount",
                "currency",
                "selected_to",
                "request_url",
                "claim_url",
                "transfer_token",
            ],
        )
        w.writeheader()
        for row in r:
            req = self.handle_request(row, options)
            if req:
                if options["dry_run"]:
                    # We can't hit .total unless the object has been saved
                    converted_amount = Decimal(row["travel_bursary"])
                    currency = req.currency
                    converted_amount = exchange(converted_amount, "USD", currency)
                else:
                    converted_amount, currency = req.total
                converted_amount = render_amount(converted_amount, currency).split(" ")[
                    0
                ]
                if options["dry_run"]:
                    request_url = "req-url"
                    transfer_token = "secret-token"
                    claim_url = "claim-url"
                else:
                    request_url = req.get_fully_qualified_url()
                    transfer = req.initiate_transfer(expire_days=options["expire_days"])
                    transfer_token = transfer.token
                    claim_url = transfer.get_fully_qualified_url()
                w.writerow(
                    {
                        "user.username": row["user.username"],
                        "converted_amount": converted_amount,
                        "currency": currency,
                        "selected_to": req.payer_group.name,
                        "request_url": request_url,
                        "claim_url": claim_url,
                        "transfer_token": transfer_token,
                    }
                )

    def create_parent_request(
        self,
        event_name: str,
        budget: str,
        owner: UserType,
        approver_group: Group,
        payer_group: Group,
        dry_run: bool,
    ):
        budget_name = pretty_budget_name(budget)
        request = Request(
            requester=owner,
            parent=None,
            state=Request.States.PENDING_APPROVAL,
            request_type=RequestType.objects.get(name="Travel"),
            description=f"{event_name} {budget_name} Travel Reimbursements",
            currency="USD",
            approver_group=approver_group,
            payer_group=payer_group,
            visible=True,
        )
        created = RequestHistory(
            request=request,
            actor=owner,
            event=RequestHistory.Events.CREATED,
        )
        self.budgets[budget] = request
        if dry_run:
            print(f"Would create parent Request: {request}")
        else:
            request.save()
            created.save()

    def handle_request(self, row: dict, options: dict):
        dry_run = options["dry_run"]
        budget = row["budget"] or "general"
        event_name = options["event_name"]
        owner = User.objects.get(username=options["owner"])
        approver_group = options["approver_group"]
        payer_group = options["payer_group"]
        creation_date = options["creation_date"]

        parent = None
        if budget:
            if budget not in self.budgets:
                self.create_parent_request(
                    event_name=event_name,
                    budget=budget,
                    owner=owner,
                    approver_group=approver_group,
                    payer_group=payer_group,
                    dry_run=dry_run,
                )
            parent = self.budgets[budget]

        country = row["user.attendee.country"]
        currency = COUNTRY_CURRENCY[country]
        payer_group = group_name(CURRENCY_PAYER[currency])
        requester_name = row["user.userprofile.display_name"]

        request = Request(
            requester=None,
            parent=parent,
            state=Request.States.APPROVED,
            request_type=RequestType.objects.get(name="Travel"),
            description=f"{event_name} Travel Reimbursements for {requester_name}",
            currency=currency,
            approver_group=approver_group,
            payer_group=payer_group,
            visible=True,
        )
        amount = Decimal(row["travel_bursary"])
        if currency != "USD":
            amount = exchange(amount, "USD", currency, creation_date.date())
        line = RequestLine(
            request=request,
            expense_type=options["expense_type"],
            description="Travel to DebConf",
            amount=amount,
        )
        created = RequestHistory(
            request=request,
            actor=owner,
            event=RequestHistory.Events.CREATED,
        )
        approved = RequestHistory(
            request=request,
            actor=owner,
            event=RequestHistory.Events.APPROVED,
            budget={},
            details="Imported approved bursaries",
        )
        if dry_run:
            print(
                f"Would create Request for {row['user.username']} in {currency} paid by {payer_group}"
            )
        else:
            request.save()
            line.save()
            created.save()
            # Override auto-now_add
            RequestHistory.objects.filter(id=created.id).update(timestamp=creation_date)
            approved.budget = request.summarize_by_type_dict()
            approved.save()
        return request
