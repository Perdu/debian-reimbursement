from collections import defaultdict
from datetime import date
from decimal import Decimal
import re

from django.conf import settings
from django.core.management.base import BaseCommand

from historical_currencies.exchange import exchange
from reimbursements.models import Request, RequestHistory


PAYER_CURRENCIES = {
    "SPI": "USD",
    "Debian.ch": "CHF",
    "Debian France": "EUR",
}

ACCOUNTS = {
    "Dc24": "expenses:travel:team:dc24",
    "Diversity": "expenses:travel:bursary:diversity",
    "DebConf Committee": "expenses:travel:team:debconf-ctte",
    "DebConf Registration Team": "expenses:travel:team:registration",
    "DebConf Video Team": "expenses:travel:team:video",
    "General": "expenses:travel:bursary:general",
    "SPI": "assets:SPI",
    "Debian.ch": "assets:debian-ch",
    "Debian France": "assets:debian-fr",
}


class Command(BaseCommand):
    help = "Summarize requests for the DebConf ledger"

    def add_arguments(self, parser):
        parser.add_argument(
            "parent-requests",
            metavar="ID",
            default=[],
            nargs="+",
            type=int,
            help="Parent requests to report from",
        )

    def handle(self, *args, **options):
        postings = {v: defaultdict(Decimal) for v in ACCOUNTS.values()}
        for request_id in options["parent-requests"]:
            request = Request.objects.get(id=request_id)
            assert request.lines.count() == 0
            expense_account = self.expense_account(request)
            for child in request.children.all():
                if child.state != Request.States.PAID:
                    continue
                assert child.children.count() == 0
                payer = child.payer_group.name
                payer_account = ACCOUNTS[payer]
                amount, currency = self.summarize_request(child)
                assert currency == PAYER_CURRENCIES[payer]
                postings[expense_account][currency] += amount
                postings[payer_account][currency] -= amount

        posting_lines = []
        for account, amounts in postings.items():
            for currency, amount in amounts.items():
                posting_lines.append(f"    {account:50}{amount:-10} {currency}")
        posting_lines.sort()

        print(f"{date.today().isoformat()} * Travel Reimbursement")
        print(f"    ; Totals exported from {settings.SITE_URL}")
        print("\n".join(posting_lines))

    def summarize_request(self, request: Request) -> tuple[Decimal, str]:
        payer = request.payer_group
        payer_currency = PAYER_CURRENCIES[payer.name]
        reimbursed = request.reimbursed_total

        reimbursement = (
            request.history.filter(event=RequestHistory.Events.REIMBURSED)
            .order_by("-timestamp")
            .first()
        )
        if reimbursement:
            reimbursement_date = reimbursement.timestamp.date()
        else:
            print(f"No reimbursement date for request {request.id}")
            reimbursement_date = None

        converted_reimbursed = exchange(
            reimbursed[0],
            reimbursed[1],
            payer_currency,
            reimbursement_date,
        )
        return (converted_reimbursed, payer_currency)

    def expense_account(self, request: Request) -> str:
        m = re.match(r"^DebConf ?\d+ (.*) Travel Reimbursements$", request.subject)
        assert m
        category = m.group(1)
        return ACCOUNTS[category]
