from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction

import requests

from org_configuration.debian.models import NMStatus


class Command(BaseCommand):
    help = "Update known Debian Contributor statuses"

    def get_statuses(self):
        r = requests.get(
            "https://nm.debian.org/api/status/",
            headers={"Api-Key": settings.DEBIAN_NM_API_KEY},
        )
        r.raise_for_status()
        return r.json()["people"]

    def handle(self, *args, **options):
        statuses = self.get_statuses()
        known_emails = set(statuses)
        existing_emails = set(NMStatus.objects.values_list("email", flat=True))

        with transaction.atomic():
            for email, status in statuses.items():
                NMStatus.objects.update_or_create(
                    email=email, defaults={"status": status["status"]}
                )

            NMStatus.objects.filter(email__in=existing_emails - known_emails).delete()
