from allauth.socialaccount.providers.gitlab.provider import (
    GitLabProvider,
)

from org_configuration.debian.socialaccount_providers.salsa.views import (
    SalsaOAuth2Adapter,
)


class SalsaProvider(GitLabProvider):
    id = "salsa"
    name = "Salsa"
    oauth2_adapter_class = SalsaOAuth2Adapter


provider_classes = [SalsaProvider]
