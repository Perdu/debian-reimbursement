from django.db import models
from django.utils.translation import gettext_lazy as _


class NMStatus(models.Model):
    objects: models.Manager["NMStatus"]

    class NMStatuses(models.TextChoices):
        dc = "dc", _("Debian Contributor")
        dc_ga = "dc_ga", _("Debian Contributor, with guest account")
        dm = "dm", _("Debian Maintainer")
        dm_ga = "dm_ga", _("Debian Maintainer, with guest account")
        dd_u = "dd_u", _("Debian Developer, uploading")
        dd_nu = "dd_nu", _("Debian Developer, non-uploading")
        emeritus_dd = "dd_e", _("Debian Developer, emeritus")
        removed_dd = "dd_r", _("Debian Developer, removed")

    email = models.EmailField(unique=True)
    status = models.CharField(
        max_length=5, choices=NMStatuses.choices, default=NMStatuses.dc
    )

    def __str__(self):
        return f"NM Status of {self.email}: {self.status}"
