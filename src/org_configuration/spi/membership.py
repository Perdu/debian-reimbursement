from django.contrib.auth.models import AbstractUser

from org_configuration.spi.models import SPIStatus


def spi_status(user: AbstractUser) -> dict[str, str]:
    """Given a user, determine their SPI Contributor Status"""
    try:
        status = SPIStatus.objects.get(username=user.username)
    except SPIStatus.DoesNotExist:
        return {
            "bootstrap_color": "danger",
            "status": "No known SPI contributor status",
        }

    bootstrap_color = {
        "nm": "secondary",
        "ncm": "info",
        "cm": "success",
    }[status.status]

    return {
        "bootstrap_color": bootstrap_color,
        "status": SPIStatus.SPIStatuses[status.status].label,
    }
