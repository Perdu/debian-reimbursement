import yaml
import re
from argparse import RawTextHelpFormatter

from django.core.management.base import BaseCommand

from django.contrib.auth.models import User, Group
from django.db.models import Q


class Command(BaseCommand):
    help = """
This script checks for changes in the projects.yaml file and pushes
these changes in the reimbursements database.

Tracked changes:
- new associated project
- removed associated project
- new project liaison
- removed project liaison

CAUTION: Some changes are hard to automate and should still be done
manually in Django:
- renaming projects
- changing a project liaison email address (not recommended)
    """
    YAML_FILE_LOCATION = "/tmp/projects.yaml"

    # Force Django to respect newline in help text
    # https://stackoverflow.com/a/35470682
    def create_parser(self, *args, **kwargs):
        parser = super().create_parser(*args, **kwargs)
        parser.formatter_class = RawTextHelpFormatter
        return parser

    def get_name(self, project):
        if "shortname" in project:
            return project["shortname"]
        else:
            return project["name"]

    def get_yaml_groups(self, data):
        yaml_groups = {}
        for i in data:
            project = data[i]
            if "liaisons" in project:
                name = self.get_name(project)
                liaisons = []
                for liaison in project["liaisons"]:
                    liaisons.append(self.extract_liaison_name_and_address(liaison)[0])
                yaml_groups[name] = liaisons
        return yaml_groups

    def extract_liaison_name_and_address(self, line):
        pattern = r"(.*?)\s*<(?P<email>[^>]+)>|(?P<email_only>[^\s]+@[^\s]+)"
        match = re.match(pattern, line)
        if match:
            name = match.group(1).strip() if match.group(1) else ""
            email = match.group("email") or match.group("email_only")
            return email, name
        else:
            print(f"Error on line {line}")
        return None, None

    def get_yaml_liaisons(self, data):
        yaml_liaisons = []
        for i in data:
            project = data[i]
            if "liaisons" in project:
                for l in project["liaisons"]:
                    email, name = self.extract_liaison_name_and_address(l)
                    if email is not None:
                        yaml_liaisons.append(
                            {
                                "email": email,
                                "name": name,
                                "group": self.get_name(project),
                            }
                        )
        return yaml_liaisons

    def split_name(self, full_name, email):
        first_name = ""
        last_name = ""
        if full_name != "":
            pattern = r"(.*?)\s+(.*)"
            match = re.match(pattern, full_name)
            if match:
                first_name = match.group(1)
                last_name = match.group(2)
            else:
                print(
                    f"Error: could not parse firstname and lastname of user {email} ({full_name})"
                )
        return first_name, last_name

    def handle(self, *args, **options):
        ## Groups
        with open(self.YAML_FILE_LOCATION) as file:
            data = yaml.safe_load(file)
        yaml_groups = self.get_yaml_groups(data)
        groups = Group.objects.all()
        groups_names = [g.name for g in groups]

        # Create missing groups
        for name in yaml_groups:
            if name not in groups_names:
                group = Group(name=name)
                group.save()
                print(f"Created group {group}")

        # Warn about obsolete groups (which are no longer in the yaml file)
        for group_name in groups_names:
            if group_name not in yaml_groups and group_name != "SPI":
                print(
                    f"The following group can be manually deleted: {group_name}. Removing its users."
                )
                group = Group.objects.get(name=group_name)
                for user in User.objects.filter(groups=group):
                    user.groups.remove(group)
                    print(f"Removing liaison {user.email} from removed project {group}")
                # Group.objects.get(name=group).delete()

        ## Users
        yaml_liaisons = self.get_yaml_liaisons(data)
        for l in yaml_liaisons:
            first_name, last_name = self.split_name(l["name"], l["email"])
            if User.objects.filter(
                Q(email=l["email"]) | Q(username=l["email"])
            ).exists():
                # user exists, has its name changed?
                if not User.objects.filter(
                    (Q(email=l["email"]) | Q(username=l["email"]))
                    & (Q(first_name=first_name) & Q(last_name=last_name))
                ).exists():
                    user = User.objects.get(
                        Q(email=l["email"]) | Q(username=l["email"])
                    )
                    old_first_name = user.first_name
                    old_last_name = user.last_name
                    user.first_name = first_name
                    user.last_name = last_name
                    user.save()
                    print(
                        f"Updated name of user {l['email']} from {old_first_name} {old_last_name} to {first_name} {last_name}"
                    )
                # user exists, is it in the right group?
                if not User.objects.filter(
                    Q(email=l["email"]) | Q(username=l["email"]),
                    groups__name=l["group"],
                ).exists():
                    # No, add it to group
                    user = User.objects.get(
                        Q(email=l["email"]) | Q(username=l["email"])
                    )
                    group = Group.objects.get(name=l["group"])
                    user.groups.add(group)
                    print(f"Added user {l['email']} to project {l['group']}")
            else:
                # user doesn't exist, create it and add it to the right group
                user = User(
                    username=l["email"],
                    email=l["email"],
                    first_name=first_name,
                    last_name=last_name,
                    password="setbysyncscriptnotasha1",
                )
                user.save()
                group = Group.objects.get(name=l["group"])
                user.groups.add(group)
                print(f"Created user {l['email']}, liaison of {l['group']}")

        # Remove old liaisons from groups
        all_users = User.objects.prefetch_related("groups")
        for user in all_users:
            user_groups = (
                user.groups.all()
            )  # This will be empty for users without groups
            for group in user_groups:
                if (
                    group != "SPI"
                    and group.name in yaml_groups
                    and user.email not in yaml_groups[group.name]
                ):
                    print(f"Removing old liaison {user.email} from project {group}")
                    user.groups.remove(group)

        # Display warning if some projects don't have any defined liaison
        groups = Group.objects.all()
        for group in groups:
            if not group.user_set.exists():
                print(f"WARNING: project {group} has no defined liaison")
