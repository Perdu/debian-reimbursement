# Generated by Django 5.1 on 2024-09-09 14:53

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="SPIStatus",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("username", models.CharField(max_length=150, unique=True)),
                (
                    "status",
                    models.CharField(
                        choices=[
                            ("nm", "Not a member"),
                            ("ncm", "Non-contributing member"),
                            ("cm", "Contributing member"),
                        ],
                        default="nm",
                        max_length=3,
                    ),
                ),
            ],
        ),
    ]
