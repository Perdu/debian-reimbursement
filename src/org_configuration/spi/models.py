from django.db import models
from django.utils.translation import gettext_lazy as _


class SPIStatus(models.Model):
    objects: models.Manager["SPIStatus"]

    class SPIStatuses(models.TextChoices):
        nm = "nm", _("Not a member")
        ncm = "ncm", _("Non-contributing member")
        cm = "cm", _("Contributing member")

    username = models.CharField(unique=True, max_length=150)
    status = models.CharField(
        max_length=3, choices=SPIStatuses.choices, default=SPIStatuses.nm
    )

    def __str__(self):
        return f"SPI Status of {self.username}: {self.status}"
