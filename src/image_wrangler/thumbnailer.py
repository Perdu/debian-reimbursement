from contextlib import ExitStack
from shutil import copyfileobj
from subprocess import check_call
from tempfile import TemporaryFile
from typing import IO

from PIL import Image


def convert_pdf_to_ppm(
    input_pdf: IO, output_ppm: IO, max_dimensions: tuple[int, int]
) -> None:
    """Convert a PDF's first page to a PPM, so we can open it in PIL."""
    with ExitStack() as stack:
        try:
            input_pdf.fileno()
        except OSError:
            pdf = stack.enter_context(
                TemporaryFile(prefix="thumbnailer", suffix=".pdf")
            )
            copyfileobj(input_pdf, pdf)
            pdf.seek(0)
            input_pdf = pdf

        check_call(
            ("pdftoppm", "-singlefile", "-scale-to", str(max(max_dimensions)), "-"),
            stdin=input_pdf,
            stdout=output_ppm,
        )


def thumbnail(
    input_img: IO,
    input_type: str,
    output_img: IO,
    output_type: str,
    max_dimensions: tuple[int, int],
) -> None:
    """
    Generate a thumbnail of input_img into output_img.

    The input and output file types are named in `input_type` and `output_type`,
    using PIL format names.

    The thumbnail will have max dimensions `max_dimensions`.
    """

    if input_type not in ("JPEG", "PDF", "PNG", "WEBP"):
        raise ValueError(f"Unsupported input_type: {input_type}")
    if output_type not in ("JPEG", "PNG", "WEBP"):
        raise ValueError(f"Unsupported output_type: {output_type}")

    with ExitStack() as stack:
        if input_type == "PDF":
            ppm = stack.enter_context(
                TemporaryFile(prefix="thumbnailer", suffix=".ppm")
            )
            convert_pdf_to_ppm(input_img, ppm, max_dimensions)
            ppm.seek(0)
            input_img = ppm
            input_type = "PPM"

        image = stack.enter_context(Image.open(input_img, formats=[input_type]))
        image.thumbnail(max_dimensions)
        image.save(output_img, output_type)
