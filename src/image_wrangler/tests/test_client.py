from io import BytesIO
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Any
from unittest import TestCase, mock
import json

from django.test import SimpleTestCase, override_settings
from django.core.files.uploadedfile import UploadedFile

from requests import Response
from requests_toolbelt import MultipartEncoder  # type: ignore

from reimbursements.storage import ReceiptsStorage
from reimbursements.models import Request

from image_wrangler.client import (
    ImageWranglerError,
    PDFBundlerClient,
    ThumbnailerClient,
    UnknownFileTypeException,
    ValidatorClient,
    guess_mime_type,
    guess_pil_type,
    pil_to_mime_type,
)
from image_wrangler.validator import InvalidImage


class FakeFieldFile:
    """Implements the same API as FieldFile"""

    name: str | None
    storage: ReceiptsStorage

    def __init__(self, name: str | None = "fake.png", content: bytes = b"PNG"):
        self.name = name
        self._content = content
        self.storage = mock.create_autospec(ReceiptsStorage)

    def open(self, mode: str = "r"):
        self._fh = BytesIO(self._content)
        return self._fh


class TestMimeTypes(TestCase):
    def test_guess_pil_html(self):
        with self.assertRaises(UnknownFileTypeException):
            guess_pil_type("foo.html")

    def test_guess_pil_png(self):
        self.assertEqual(guess_pil_type("foo.png"), "PNG")

    def test_png_mime_type(self):
        self.assertEqual(pil_to_mime_type("PNG"), "image/png")

    def test_pdf_mime_type(self):
        self.assertEqual(pil_to_mime_type("PDF"), "application/pdf")

    def test_guess_mime_html(self):
        with self.assertRaises(UnknownFileTypeException):
            guess_mime_type("foo.html")

    def test_guess_mime_png(self):
        self.assertEqual(guess_mime_type("foo.png"), "image/png")


class ThumbnailerClientTestMixin:
    def create_tc(self):
        self.file = FakeFieldFile()
        self.tc = ThumbnailerClient(self.file, (101, 102))

        def fake_save(name, content):
            self.assertEqual(content.read(), b"WEBP")
            return "saved-thumbnail"

        self.tc.storage.save.side_effect = fake_save
        self.tc.storage.thumbnail_path.return_value = "fake-thumbnail-path"


@override_settings(IMAGE_WRANGLER_URL=None)
class TestThumbnailerClientLocal(ThumbnailerClientTestMixin, SimpleTestCase):
    def setUp(self):
        def fake_thumbnail(
            input_img, input_type, output_img, output_type, max_dimensions
        ):
            output_img.write(b"WEBP")

        self.method_patcher = mock.patch(
            "image_wrangler.thumbnailer.thumbnail",
            autospec=True,
            side_effect=fake_thumbnail,
        )
        self.method = self.method_patcher.start()
        self.addCleanup(self.method_patcher.stop)

        self.create_tc()

    def test_create_defaults(self):
        self.assertEqual(self.tc.max_dimensions, (101, 102))
        self.assertEqual(self.tc.input_type, "PNG")
        self.assertEqual(self.tc.output_type, "WEBP")
        self.assertEqual(self.tc.storage, self.file.storage)

    def test_create_with_unsaved_fieldfile(self):
        with self.assertRaises(AssertionError):
            ThumbnailerClient(FakeFieldFile(name=None))

    def test_thumbnail_path(self):
        self.assertEqual(self.tc.thumbnail_path(), "fake-thumbnail-path")
        self.tc.storage.thumbnail_path.assert_called_once_with(
            name="fake.png", max_dimensions=(101, 102), output_type="WEBP"
        )

    def test_generate_thumbnail(self):
        self.assertEqual(self.tc.generate_thumbnail(), "saved-thumbnail")
        self.method.assert_called_once_with(
            self.file._fh, "PNG", mock.ANY, "WEBP", (101, 102)
        )
        self.file.storage.save.assert_called_once_with("fake-thumbnail-path", mock.ANY)


@override_settings(IMAGE_WRANGLER_URL="https://example.net/wrangler")
class TestThumbnailerClientRemote(ThumbnailerClientTestMixin, SimpleTestCase):
    def setUp(self):
        self.response = mock.create_autospec(Response)
        self.response.status_code = 200
        self.response.raw = BytesIO(b"WEBP")
        self.post_patcher = mock.patch(
            "requests.post", autospec=True, return_value=self.response
        )
        self.post = self.post_patcher.start()
        self.addCleanup(self.post_patcher.stop)

        self.create_tc()

    def test_generate_thumbnail(self):
        self.assertEqual(self.tc.generate_thumbnail(), "saved-thumbnail")

        self.post.assert_called_once_with(
            "https://example.net/thumbnail/WEBP/101x102/",
            headers={"Content-Type": "image/png"},
            data=self.file._fh,
            stream=True,
        )
        self.file.storage.save.assert_called_once_with("fake-thumbnail-path", mock.ANY)

    def test_generate_thumbnail_400(self):
        self.response.status_code = 400
        with self.assertRaises(ImageWranglerError):
            self.tc.generate_thumbnail()


class PDFBundlerClientTestMixin:
    def create_pc(self):
        manifest = {"content": [{"filename": "fake.pdf"}]}
        self.request = mock.create_autospec(Request)()
        self.request.pk = 42
        self.files = [FakeFieldFile(name="fake.pdf")]
        self.pc = PDFBundlerClient(manifest, self.request, self.files)

        storage = mock.create_autospec(ReceiptsStorage)

        def fake_save(name, content):
            self.assertEqual(content.read(), b"PDF")
            return "saved-pdf"

        storage.save.side_effect = fake_save
        self.pc.storage = storage


@override_settings(
    IMAGE_WRANGLER_URL=None,
    BUNDLE_FILENAME="bundle-{instance.pk}.pdf",
)
class TestPDFBundlerClientLocal(PDFBundlerClientTestMixin, SimpleTestCase):
    def setUp(self):
        def fake_bundle(manifest: dict[str, Any], work_dir: Path) -> Path:
            for item in manifest["content"]:
                if "filename" in item:
                    self.assertTrue((work_dir / item["filename"]).exists())
            bundle = work_dir / "bundle.pdf"
            bundle.write_bytes(b"PDF")
            return bundle

        self.method_patcher = mock.patch(
            "image_wrangler.pdf_bundler.bundle_pdf",
            autospec=True,
            side_effect=fake_bundle,
        )
        self.method = self.method_patcher.start()
        self.addCleanup(self.method_patcher.stop)

        self.create_pc()

    def test_create_defaults(self):
        self.assertIn("content", self.pc.manifest)
        self.assertEqual(self.pc.files, self.files)
        self.assertEqual(self.pc.request, self.request)
        self.assertEqual(
            self.pc.manifest, {"content": [{"filename": "fake.pdf", "pil_type": "PDF"}]}
        )

    def test_extend_manifest_file(self):
        self.assertEqual(
            self.pc.extend_manifest(
                {
                    "content": [
                        {"filename": "example.pdf"},
                    ]
                }
            ),
            {
                "content": [
                    {
                        "filename": "example.pdf",
                        "pil_type": "PDF",
                    },
                ]
            },
        )

    def test_extend_manifest_url(self):
        manifest = self.pc.extend_manifest(
            {
                "content": [
                    {"url": "http://example.net/page"},
                ]
            }
        )

        self.assertEqual(manifest["content"][0]["url"], "http://example.net/page")
        self.assertIn("access_key", manifest["content"][0])

    def test_bundle_path(self):
        self.assertEqual(self.pc.bundle_path(), "bundle-42.pdf")

    def test_bundle_with_unsaved_fieldfile(self):
        self.pc.files = [FakeFieldFile(name=None)]
        with self.assertRaises(AssertionError):
            self.pc.bundle_pdf()

    def test_bundle_pdf(self):
        with NamedTemporaryFile(prefix="test_bundle_pdf", suffix=".pdf") as file_1:
            self.files[0].path = Path(file_1.name)
            self.assertEqual(self.pc.bundle_pdf(), "saved-pdf")
        self.method.assert_called_once_with(self.pc.manifest, mock.ANY)
        self.pc.storage.save.assert_called_once_with("bundle-42.pdf", mock.ANY)


@override_settings(
    IMAGE_WRANGLER_URL="https://example.net/wrangler",
    BUNDLE_FILENAME="bundle-{instance.pk}.pdf",
)
class TestPDFBundlerClientRemote(PDFBundlerClientTestMixin, SimpleTestCase):
    def setUp(self):
        self.response = mock.create_autospec(Response)
        self.response.status_code = 200
        self.response.raw = BytesIO(b"PDF")
        self.post_patcher = mock.patch(
            "requests.post", autospec=True, return_value=self.response
        )
        self.post = self.post_patcher.start()
        self.addCleanup(self.post_patcher.stop)

        self.create_pc()

    def test_bundle_pdf(self):
        with NamedTemporaryFile(prefix="test_bundle_pdf", suffix=".pdf") as file_1:
            self.files[0].path = Path(file_1.name)
            self.assertEqual(self.pc.bundle_pdf(), "saved-pdf")

        self.post.assert_called_once_with(
            "https://example.net/bundle-pdf/",
            data=mock.ANY,
            headers=mock.ANY,
            stream=True,
        )
        data = self.post.call_args.kwargs["data"]
        self.assertIsInstance(data, MultipartEncoder)
        self.assertIn("fake.pdf", data.fields)
        self.assertEqual(
            data.fields["fake.pdf"], ("fake.pdf", self.files[0]._fh, "application/pdf")
        )
        self.assertEqual(data.fields["manifest"], json.dumps(self.pc.manifest))

        headers = self.post.call_args.kwargs["headers"]
        self.assertIn("Content-Type", headers)
        self.assertRegex(headers["Content-Type"], r"multipart/form-data; boundary=\w+")
        self.pc.storage.save.assert_called_once_with("bundle-42.pdf", mock.ANY)

    def test_bundle_pdf_400(self):
        self.response.status_code = 400
        self.files = []
        with self.assertRaises(ImageWranglerError):
            self.pc.bundle_pdf()


@override_settings(IMAGE_WRANGLER_URL=None)
class TestValidatorClientLocal(SimpleTestCase):
    def setUp(self):
        self.method_patcher = mock.patch(
            "image_wrangler.validator.validate_img",
            autospec=True,
        )
        self.method = self.method_patcher.start()
        self.addCleanup(self.method_patcher.stop)

        self.file = UploadedFile(name="fake.png", file=BytesIO(b"PNG"))
        self.vc = ValidatorClient(self.file)

    def test_create_defaults(self):
        self.assertEqual(self.vc.img, self.file)
        self.assertEqual(self.vc.pil_type, "PNG")

    def test_create_with_unsaved_fieldfile(self):
        with self.assertRaises(AssertionError):
            ValidatorClient(UploadedFile(name=None))

    def test_validate(self):
        self.assertEqual(self.vc.validate(), True)
        self.method.assert_called_once_with(self.file, "PNG")

    def test_validate_fails(self):
        self.method.side_effect = InvalidImage
        self.assertEqual(self.vc.validate(), False)


@override_settings(IMAGE_WRANGLER_URL="https://example.net/wrangler")
class TestValidatorClientRemote(ThumbnailerClientTestMixin, SimpleTestCase):
    def setUp(self):
        self.response = mock.create_autospec(Response)
        self.response.status_code = 204
        self.post_patcher = mock.patch(
            "requests.post", autospec=True, return_value=self.response
        )
        self.post = self.post_patcher.start()
        self.addCleanup(self.post_patcher.stop)

        self.file = UploadedFile(name="fake.png", file=BytesIO(b"PNG"))
        self.vc = ValidatorClient(self.file)

    def test_generate_thumbnail(self):
        self.assertEqual(self.vc.validate(), True)

        self.post.assert_called_once_with(
            "https://example.net/validate/",
            headers={"Content-Type": "image/png"},
            data=self.file,
            stream=True,
        )

    def test_generate_thumbnail_400(self):
        self.response.status_code = 400
        self.assertEqual(self.vc.validate(), False)

    def test_generate_thumbnail_500(self):
        self.response.status_code = 500
        with self.assertRaises(ImageWranglerError):
            self.vc.validate()
