import time
from unittest import TestCase, mock

from django.contrib.auth.models import AnonymousUser, User
from django.core.exceptions import PermissionDenied
from django.test import RequestFactory

from image_wrangler.authentication import (
    MSG,
    LoginOrAccessKeyRequiredMixin,
    _signer,
    generate_access_key,
    validate_access_key,
)


class TestSigner(TestCase):
    def test_creation(self):
        signer = _signer()
        self.assertEqual(signer.sep, "~")


class TestAccessKeys(TestCase):
    def test_create(self):
        key = generate_access_key()
        self.assertIsInstance(key, str)
        self.assertTrue(key.startswith(MSG))
        self.assertTrue(len(key) > len(MSG))

    def test_validate(self):
        key = generate_access_key()
        self.assertTrue(validate_access_key(key))

    def test_validate_corrupted_sig(self):
        key = generate_access_key()[:-5] + "xxxxx"
        self.assertFalse(validate_access_key(key))

    def test_validate_corrupted_msg(self):
        key = "xxxxx" + generate_access_key()[5:]
        self.assertFalse(validate_access_key(key))

    def test_validate_wrong_msg(self):
        key = _signer().sign("wrong-message")
        self.assertFalse(validate_access_key(key))

    def test_validate_tilde_msg(self):
        self.assertFalse(validate_access_key("~~~~~~~"))

    def test_validate_expired(self):
        with mock.patch("time.time", return_value=time.time() - 1):
            key = generate_access_key()
        self.assertFalse(validate_access_key(key, expiry=1))


class TestAccessKeyAcceptedMixin(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = LoginOrAccessKeyRequiredMixin()

    def test_no_header(self):
        self.view.request = self.factory.get("/")
        self.assertFalse(self.view.valid_access_key())

    def test_invalid_header_format(self):
        self.view.request = self.factory.get("/", headers={"Authorization": "foo"})
        self.assertFalse(self.view.valid_access_key())

    def test_header_no_bearer(self):
        self.view.request = self.factory.get("/", headers={"Authorization": "Bearer"})
        self.assertFalse(self.view.valid_access_key())

    def test_invalid_header(self):
        self.view.request = self.factory.get(
            "/", headers={"Authorization": "Bearer " + "x" * 30}
        )
        self.assertFalse(self.view.valid_access_key())

    def test_valid_header(self):
        self.view.request = self.factory.get(
            "/", headers={"Authorization": "Bearer " + generate_access_key()}
        )
        self.assertTrue(self.view.valid_access_key())


class TestLoginOrAccessKeyRequiredMixin(TestCase):
    def setUp(self):
        class View:
            dispatched = False

            def dispatch(self, request, *args, **kwargs):
                self.dispatched = True

        class TestView(LoginOrAccessKeyRequiredMixin, View):
            raise_exception = True

        self.view = TestView()
        self.factory = RequestFactory()

    def test_anon_denied(self):
        self.view.request = self.factory.get("/")
        self.view.request.user = AnonymousUser()
        with self.assertRaises(PermissionDenied):
            self.view.dispatch(self.view.request)
        self.assertFalse(self.view.request.valid_access_key)
        self.assertFalse(self.view.dispatched)

    def test_cookie_accepted(self):
        self.view.request = self.factory.get("/")
        self.view.request.user = User()
        self.view.dispatch(self.view.request)
        self.assertFalse(self.view.request.valid_access_key)
        self.assertTrue(self.view.dispatched)

    def test_token_accepted(self):
        self.view.request = self.factory.get(
            "/", headers={"Authorization": "Bearer " + generate_access_key()}
        )
        self.view.request.user = AnonymousUser()
        self.view.dispatch(self.view.request)
        self.assertTrue(self.view.request.valid_access_key)
        self.assertTrue(self.view.dispatched)
