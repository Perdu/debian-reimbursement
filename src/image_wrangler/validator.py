import json
from shutil import copyfileobj
from subprocess import CalledProcessError, check_output
from tempfile import NamedTemporaryFile
from typing import IO

from PIL import Image, UnidentifiedImageError


class InvalidImage(Exception):
    pass


def validate_img(img: IO, pil_type: str):
    """Validate that img is a valid file of pil_type"""
    if pil_type == "PDF":
        with NamedTemporaryFile(prefix="pdf-validate-", suffix=".pdf") as f:
            copyfileobj(img, f)
            f.flush()
            try:
                parsed = json.loads(
                    check_output(
                        ["qpdf", "--warning-exit-0", "--json", f.name], errors="ignore"
                    )
                )
            except CalledProcessError:
                raise InvalidImage("qpdf could not parse the PDF")
            if parsed["encrypt"]["encrypted"]:
                raise InvalidImage("PDF is encrypted")
        return

    try:
        with Image.open(img, formats=[pil_type]) as im:
            im.load()
    except UnidentifiedImageError as e:
        raise InvalidImage(f"Unable to parse image: {e}")
