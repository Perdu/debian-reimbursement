import json
from contextlib import ExitStack
from copy import deepcopy
from pathlib import Path
from shutil import copyfile, copyfileobj
from tempfile import TemporaryDirectory, TemporaryFile
from typing import IO, TYPE_CHECKING, Any, cast
from urllib.parse import urljoin

from django.conf import settings
from django.core.files.uploadedfile import UploadedFile
from django.core.files.storage import Storage
from django.db.models.fields.files import FieldFile

import requests
from requests_toolbelt import MultipartEncoder

from image_wrangler.authentication import generate_access_key
from reimbursements.storage import (
    ReceiptsStorage,
    generate_bundle_filename,
    receipts_storage,
)

if TYPE_CHECKING:  # pragma: no cover
    from reimbursements.models import Request


class UnknownFileTypeException(Exception):
    pass


class ImageWranglerError(Exception):
    pass


def guess_pil_type(filename: str) -> str:
    """Guess the PIL filetype of filename."""
    extension = Path(filename).suffix.lower()
    types = {
        ".jpeg": "JPEG",
        ".jpg": "JPEG",
        ".pdf": "PDF",
        ".png": "PNG",
        ".webp": "WEBP",
    }
    if extension not in types:
        raise UnknownFileTypeException(
            f"Unsupported file type {extension} (for {filename})"
        )
    return types[extension]


def pil_to_mime_type(pil_type: str) -> str:
    """Convert a PIL filetype to a MIME Type."""
    if pil_type == "PDF":
        maintype = "application"
    else:
        maintype = "image"
    subtype = pil_type.lower()
    return f"{maintype}/{subtype}"


def guess_mime_type(filename: str) -> str:
    """Guess a MIME Type of a filename."""
    return pil_to_mime_type(guess_pil_type(filename))


class ThumbnailerClient:
    """
    Generate a thumbnail of input_img of max_dimensions.

    output_type is a PIL file type.

    If a remote image wrangler service is configured, all the image handling
    work will be done there.
    """

    input_img: FieldFile
    input_type: str
    output_type: str
    max_dimensions: tuple[int, int]
    storage: Storage

    def __init__(
        self,
        input_img: FieldFile,
        max_dimensions: tuple[int, int] = (100, 100),
        output_type: str = "WEBP",
    ):
        self.input_img = input_img
        self.max_dimensions = max_dimensions
        self.output_type = output_type
        assert isinstance(input_img.name, str)
        self.input_type = guess_pil_type(input_img.name)
        self.storage = self.input_img.storage

    def thumbnail_path(self):
        """Return the thumbnail's path."""
        return self.storage.thumbnail_path(
            name=self.input_img.name,
            max_dimensions=self.max_dimensions,
            output_type=self.output_type,
        )

    def generate_thumbnail(self) -> str:
        """
        Generate the thumbnail.

        Return the stored filename.
        """
        output_storage_name = self.thumbnail_path()
        with TemporaryFile() as out_f:
            with self.input_img.open() as in_f:
                self._call_thumbnailer(in_f, out_f)
            out_f.seek(0)
            # Delete any existing thumbnail (no error raised if it doesn't exist)
            self.storage.delete(output_storage_name)
            return self.storage.save(output_storage_name, out_f)

    def _call_thumbnailer(self, input_img: IO, output_img: IO):
        """Call a local/remote thumbnailer to do the work."""
        url = settings.IMAGE_WRANGLER_URL
        if url is None:
            from image_wrangler.thumbnailer import thumbnail

            return thumbnail(
                input_img,
                self.input_type,
                output_img,
                self.output_type,
                self.max_dimensions,
            )

        dimensions = "{}x{}".format(*self.max_dimensions)
        url = urljoin(url, f"thumbnail/{self.output_type}/{dimensions}/")
        r = requests.post(
            url,
            headers={"Content-Type": pil_to_mime_type(self.input_type)},
            data=input_img,
            stream=True,
        )
        if r.status_code != requests.codes.ok:
            raise ImageWranglerError(
                f"Unexpected response from thumbnailer: {r.status_code}"
            )
        copyfileobj(r.raw, output_img)


class PDFBundlerClient:
    """
    Generate a PDF bundling up multiple assets.

    If a remote image wrangler service is configured, all the image handling
    work will be done there.
    """

    manifest: dict[str, Any]
    files: list[FieldFile]
    request: "Request"
    storage: ReceiptsStorage

    def __init__(
        self,
        manifest: dict[str, Any],
        request: "Request",
        files: list[FieldFile],
    ):
        self.manifest = self.extend_manifest(manifest)
        self.files = files
        self.request = request
        self.storage = receipts_storage

    def extend_manifest(self, manifest: dict[str, Any]) -> dict[str, Any]:
        """Include some extra data that the bundler needs in the manifest"""
        output = deepcopy(manifest)
        for item in output["content"]:
            if "filename" in item:
                item["pil_type"] = guess_pil_type(item["filename"])
            if "url" in item:
                item["access_key"] = generate_access_key()
        return output

    def bundle_path(self):
        """Return the bundles's path."""
        return generate_bundle_filename(instance=self.request)

    def bundle_pdf(self) -> str:
        """
        Generate the PDF Bundle.

        Return the stored filename.
        """
        url = settings.IMAGE_WRANGLER_URL
        if url is None:
            return self.bundle_pdf_locally()
        else:
            return self.bundle_pdf_remotely(url)

    def bundle_pdf_locally(self) -> str:
        """
        Prepare a temporary directory to bundle the PDF in and call the bundler.
        """
        from image_wrangler.pdf_bundler import bundle_pdf

        output_storage_name = self.bundle_path()
        with TemporaryDirectory(prefix="pdf-bundle") as temp_dir:
            temp_path = Path(temp_dir)
            for file in self.files:
                assert isinstance(file.name, str)
                copyfile(file.path, temp_path / file.name)
            result = bundle_pdf(self.manifest, temp_path)

            # Delete any existing bundle (no error raised if it doesn't exist)
            self.storage.delete(output_storage_name)
            with result.open("rb") as out_f:
                return self.storage.save(output_storage_name, out_f)

    def bundle_pdf_remotely(self, url: str) -> str:
        """Request a remote bundling."""
        url = urljoin(url, "bundle-pdf/")
        data: dict[str, str | tuple[str, IO, str]] = {}
        output_storage_name = self.bundle_path()

        with ExitStack() as stack:
            for file in self.files:
                assert isinstance(file.name, str)
                data[file.name] = (
                    file.name,
                    stack.enter_context(file.open("rb")),
                    guess_mime_type(file.name),
                )
            data["manifest"] = json.dumps(self.manifest)
            encoder = MultipartEncoder(data)
            r = requests.post(
                url,
                headers={"Content-Type": encoder.content_type},
                data=encoder,
                stream=True,
            )
            if r.status_code != requests.codes.ok:
                raise ImageWranglerError(
                    f"Unexpected response from PDF bundler: {r.status_code}"
                )
            # Delete any existing bundle (no error raised if it doesn't exist)
            self.storage.delete(output_storage_name)
        return self.storage.save(output_storage_name, cast(IO[bytes], r.raw))


class ValidatorClient:
    """
    Validate input_img.

    If a remote image wrangler service is configured, all the image handling
    work will be done there.
    """

    img: UploadedFile
    pil_type: str

    def __init__(
        self,
        img: UploadedFile,
    ):
        self.img = img
        assert isinstance(img.name, str)
        self.pil_type = guess_pil_type(img.name)

    def validate(self) -> bool:
        """Validate the image."""
        url = settings.IMAGE_WRANGLER_URL
        if url is None:
            from image_wrangler.validator import InvalidImage, validate_img

            try:
                validate_img(self.img, self.pil_type)
                return True
            except InvalidImage:
                return False

        url = urljoin(url, "validate/")
        r = requests.post(
            url,
            headers={"Content-Type": pil_to_mime_type(self.pil_type)},
            data=self.img,
            stream=True,
        )
        self.img.seek(0)
        if r.status_code == 204:
            return True
        if r.status_code == 400:
            return False
        if r.status_code != requests.codes.ok:
            raise ImageWranglerError(
                f"Unexpected response from validator: {r.status_code}"
            )
