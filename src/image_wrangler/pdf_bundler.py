from pathlib import Path
from subprocess import check_call
from typing import Any

from PIL import Image


class PDFBundler:
    manifest: dict[str, Any]
    workdir: Path
    _content: list[dict[str, Any]]

    def __init__(self, manifest: dict[str, Any], workdir: Path):
        self.manifest = manifest
        self.workdir = workdir

    def parse_manifest(self):
        """Parse the manifest dict into individual class attributes."""
        self._content = self.manifest["content"]

    def html_to_pdf(self, url: str, access_key: str, output: Path):
        """Render url to a PDF stored at output."""
        assert not output.exists()
        cmd = [
            "wkhtmltopdf",
            "--custom-header",
            "Authorization",
            f"Bearer {access_key}",
            "--custom-header-propagation",
            # https://github.com/wkhtmltopdf/wkhtmltopdf/issues/1737
            "--run-script",
            "prepareToPrint()",
            "--debug-javascript",
            "--quiet",
            url,
            str(output),
        ]
        check_call(cmd)

    def image_to_pdf(self, image: Path, input_type, output: Path):
        """Convert an image to a PDF."""
        with Image.open(image, formats=[input_type]) as im:
            im.save(output, "PDF", resolution=300)

    def concatenate_pdfs(self, pdfs: list[Path], output: Path):
        """Concatenate pdfs into output."""
        assert not output.exists()
        cmd = ["pdfunite"] + [str(pdf) for pdf in pdfs] + [str(output)]
        check_call(cmd)

    def bundle(self) -> Path:
        """Do all the work."""
        self.parse_manifest()
        target = self.workdir / "bundle.pdf"
        converted = self.workdir / "converted"
        converted.mkdir()
        pdfs = []
        for i, item in enumerate(self._content):
            converted_name = converted / f"{i}.pdf"

            if "filename" not in item:
                self.html_to_pdf(item["url"], item["access_key"], converted_name)
                pdfs.append(converted_name)
                continue

            assert "/" not in item["filename"]
            assert not item["filename"].startswith(".")

            item_path = self.workdir / item["filename"]

            if item.get("pil_type") == "PDF":
                pdfs.append(self.workdir / item["filename"])
                continue

            pil_type = item.get("pil_type")
            if pil_type not in ("JPEG", "PNG", "WEBP"):
                raise ValueError(f"Unsupported pil_type: {pil_type} {item}")
            self.image_to_pdf(item_path, pil_type, converted_name)
            pdfs.append(converted_name)

        self.concatenate_pdfs(pdfs, target)
        return target


def bundle_pdf(manifest: dict[str, Any], workdir: Path) -> Path:
    """
    Build a PDF out of manifest, using files stored in workdir.
    Return the path to the built PDF.
    """
    return PDFBundler(manifest, workdir).bundle()
