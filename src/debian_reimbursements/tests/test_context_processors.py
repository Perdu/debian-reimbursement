from django.test import RequestFactory, SimpleTestCase, override_settings

from debian_reimbursements.context_processors import meta


class TestMetaProcessor(SimpleTestCase):
    def test_meta(self):
        factory = RequestFactory()
        with override_settings(
            SITE_NAME="name",
            SITE_PROD_STATE="test",
            SITE_URL="https://example.com/",
        ):
            r = meta(factory.get("/"))
        self.assertEqual(
            r,
            {
                "SITE_NAME": "name",
                "SITE_PROD_STATE": "test",
                "SITE_URL": "https://example.com/",
            },
        )
