import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import FormView

from reimbursements.forms.bank_details import label_bank_details
from reimbursements.models import Profile, Request
from webui.forms import UserEditForm

from django.conf import settings

log = logging.getLogger(__name__)


class IndexView(TemplateView):
    template_name = "webui/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if hasattr(settings, "DEPLOYMENT"):
            context["DEPLOYMENT"] = settings.DEPLOYMENT
        else:
            context["DEPLOYMENT"] = None
        return context


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = "webui/profile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        reviewable = {}
        payable = {}
        for group in self.request.user.groups.all():
            requests = group.requests_approver.filter(
                state=Request.States.PENDING_APPROVAL
            )
            if requests.exists():
                reviewable[group.name] = requests.all()
            requests = group.requests_payer.filter(state=Request.States.PENDING_PAYMENT)
            if requests.exists():
                payable[group.name] = requests.all()
        context["reviewable"] = reviewable
        context["payable"] = payable
        return context


class ProfileEditView(LoginRequiredMixin, FormView):
    template_name = "webui/profile_edit.html"
    form_class = UserEditForm

    def get_initial(self):
        user = self.request.user
        return {
            "username": user.username,
            "name": user.get_full_name(),
        }

    def get_object(self):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        salsa_account = self.request.user.socialaccount_set.filter(provider="salsa")
        context["salsa_account"] = salsa_account.exists()
        if salsa_account.exists():
            salsa_account = salsa_account.first()
            context["salsa_uid"] = salsa_account.uid
            context["salsa_username"] = salsa_account.extra_data["username"]
            context["salsa_web_url"] = salsa_account.extra_data["web_url"]

        try:
            bank_details = self.request.user.profile.bank_details
        except Profile.DoesNotExist:
            context["bank_details"] = None
        else:
            context["bank_details"] = label_bank_details(bank_details)

        return context

    def form_valid(self, form):
        user = self.request.user
        for k, v in self.get_initial().items():
            if v != form.cleaned_data[k]:
                if k == "username":
                    log.info(
                        "User %s changed their username to %s", v, form.cleaned_data[k]
                    )

                if k == "name":
                    split = form.cleaned_data["name"].partition(" ")
                    user.first_name = split[0]
                    user.last_name = split[2]
                else:
                    setattr(user, k, form.cleaned_data[k])

        user.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("profile")


class DeleteBankDetailsView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        try:
            profile = request.user.profile
        except Profile.DoesNotExist:
            pass
        else:
            profile.bank_details = {}
            profile.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("profile-edit")
