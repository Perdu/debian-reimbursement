from datetime import timedelta
from decimal import Decimal

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.files import File
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.utils import crypto, timezone

from historical_currencies.exchange import exchange
from image_wrangler.client import PDFBundlerClient
from reimbursements.storage import generate_bundle_filename, receipts_storage

from reimbursements.models.history import RequestHistory
from reimbursements.models.reimbursement import ReimbursementLine
from reimbursements.models.types import RequestType, ExpenseTypeAllocation

User = get_user_model()


class Request(models.Model):
    class States(models.TextChoices):
        DRAFT = "draft", _("Draft")
        PENDING_APPROVAL = "pending_approval", _("Pending Approval")
        APPROVED = "approved", _("Approved")
        DELETED = "deleted", _("Deleted")
        PENDING_PAYMENT = "pending_payment", _("Pending Payment")
        PAID = "paid", _("Paid")

    requester = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)
    parent = models.ForeignKey(
        "Request",
        on_delete=models.PROTECT,
        null=True,
        related_name="children",
        blank=True,
    )
    state = models.CharField(
        max_length=16, choices=States.choices, default=States.DRAFT
    )
    bank_details = models.JSONField(null=True, blank=True)
    request_type = models.ForeignKey(RequestType, on_delete=models.PROTECT)
    description = models.TextField()
    expected_date = models.DateField(null=True, blank=True)
    currency = models.CharField(max_length=3, default=settings.DEFAULT_CURRENCY)
    approver_group = models.ForeignKey(
        Group, on_delete=models.PROTECT, null=True, related_name="requests_approver"
    )
    payer_group = models.ForeignKey(
        Group, on_delete=models.PROTECT, related_name="requests_payer"
    )
    visible = models.BooleanField(default=False)

    @property
    def subject(self):
        lines = self.description.splitlines()
        if lines:
            return lines[0].strip()
        return ""

    @property
    def total(self):
        return (
            self.lines.aggregate(total=models.Sum("amount"))["total"] or Decimal(0),
            self.currency,
        )

    @property
    def receipts_total(self):
        from reimbursements.models.receiptline import ReceiptLine

        lines = ReceiptLine.objects.filter(receipt__request=self)
        return (
            lines.aggregate(total=models.Sum("converted_amount"))["total"]
            or Decimal(0),
            self.currency,
        )

    @property
    def reimbursed_total(self):
        lines = ReimbursementLine.objects.filter(request=self)
        return (
            lines.aggregate(total=models.Sum("amount"))["total"] or Decimal(0),
            self.currency,
        )

    @property
    def created(self):
        try:
            return self.history.get(event=RequestHistory.Events.CREATED).timestamp
        except RequestHistory.DoesNotExist:
            return None

    @property
    def submitted(self):
        try:
            return self.history.get(event=RequestHistory.Events.SUBMITTED).timestamp
        except RequestHistory.DoesNotExist:
            return None

    @property
    def updated(self):
        last_event = self.history.all().last()
        if last_event:
            return last_event.timestamp
        return None

    def get_absolute_url(self):
        return reverse("request", kwargs={"pk": self.pk})

    def get_fully_qualified_url(self):
        return settings.SITE_URL.rstrip("/") + self.get_absolute_url()

    def summarize_by_type(self):
        summary = {}
        total = ExpenseTypeAllocation("Total", self.currency, special=True)
        for line in self.lines.all():
            if line.expense_type_id not in summary:
                summary[line.expense_type_id] = ExpenseTypeAllocation(
                    line.expense_type.name, self.currency
                )
            summary[line.expense_type_id].requested += line.amount
            total.requested += line.amount
        for line in self.reimbursement_lines.all():
            if line.expense_type_id not in summary:
                summary[line.expense_type_id] = ExpenseTypeAllocation(
                    line.expense_type.name, self.currency
                )
            summary[line.expense_type_id].reimbursed += line.amount
            total.reimbursed += line.amount
        for receipt in self.receipts.all():
            for line in receipt.lines.all():
                if line.expense_type_id not in summary:
                    summary[line.expense_type_id] = ExpenseTypeAllocation(
                        line.expense_type.name, self.currency
                    )
                summary[line.expense_type_id].spent += line.converted_amount
                total.spent += line.converted_amount
        summary["total"] = total
        return summary

    def summarize_by_type_dict(self):
        allocation = {}
        for k, v in self.summarize_by_type().items():
            allocation[v.name] = v.to_dict()
        return allocation

    def summarize_by_beancount_type(self):
        summary = {}
        total = ExpenseTypeAllocation("Liabilities:Payable", "USD", special=True)
        if self.expected_date:
            date = self.expected_date
        else:
            date = self.submitted
        for line in self.lines.all():
            if line.expense_type_id not in summary:
                summary[line.expense_type_id] = ExpenseTypeAllocation(
                    line.expense_type.beancount_name, self.currency
                )
            converted_amount = exchange(line.amount, self.currency, "USD", date)
            summary[line.expense_type_id].requested += converted_amount
            total.requested -= converted_amount
        summary["total"] = total
        return summary

    @property
    def approved_budget_limit(self):
        """The total requested, plus acceptable leeway"""
        total = self.total[0]
        approved_limit = total * (
            1 + Decimal(settings.OVERBUDGET_LEEWAY_PERCENTAGE) / 100
        )
        return (approved_limit, self.currency)

    @property
    def total_reimburseable(self):
        """The total requested, cut off at approved_budget_limit"""
        approved_total = min(self.approved_budget_limit[0], self.receipts_total[0])
        return (approved_total, self.currency)

    @property
    def within_budget(self):
        budget = self.approved_budget_limit[0]
        receipts_total = self.receipts_total[0]
        return receipts_total <= budget

    @property
    def over_budget(self):
        return not self.within_budget

    def __str__(self):
        return f"Request {self.id}: {self.subject}"

    def user_access(self, user, access_key=False):
        """What access levels does user have to this Request"""
        access = {}
        if self.requester == user and self.requester is not None:
            access["requester"] = True
        if user.is_superuser:
            access["admin"] = True
        if self.visible:
            if user.groups.filter(id=self.approver_group_id).exists():
                access["approver"] = True
            if user.groups.filter(id=self.payer_group_id).exists():
                access["payer"] = True
        if access_key:
            access["access_key"] = True
        return access

    @property
    def requester_name(self) -> str | None:
        """Helper for templates, to render the requester's name"""
        if not self.requester:
            return "Currently Unclaimed"
        name = self.requester.get_full_name()
        if not name:
            name = self.requester.username
        return name

    def generate_pdf_bundle(self):
        """Create a PDF bundle of the request"""
        content = [
            {"url": self.get_fully_qualified_url()},
        ]
        files = []
        for receipt in self.receipts.all():
            content.append({"filename": receipt.file.name})
            files.append(receipt.file)
        manifest = {"content": content}
        PDFBundlerClient(manifest, self, files).bundle_pdf()

    @property
    def pdf_bundle(self) -> File:
        """Open the PDF bundle file and return it."""
        return receipts_storage.open(generate_bundle_filename(self))

    def delete_pdf_bundle(self):
        """Delete any existing PDF bundle."""
        return receipts_storage.delete(generate_bundle_filename(self))

    def initiate_transfer(self, expire_days=60):
        from reimbursements.models.transfer import RequestTransfer

        transfer = RequestTransfer(
            request=self,
            expires=timezone.now() + timedelta(days=expire_days),
            token=crypto.get_random_string(length=20),
        )
        transfer.save()
        return transfer

    @property
    def pretty_bank_details(self) -> dict[str, str] | None:
        from reimbursements.forms.bank_details import label_bank_details

        if self.bank_details:
            return label_bank_details(self.bank_details)
        return None

    @property
    def generate_beancount_report(self):
        content = ""
        content += self.created.strftime('%Y-%m-%d')
        content += " * "
        content += '"' + self.requester.first_name + " " + self.requester.last_name + '"'
        content += " "
        content += '"' + self.description.split('\n', 1)[0].strip() + '"'
        rt_tickets = self.rt_tickets.all()
        if len(rt_tickets) > 0:
            content += "\n"
            content += "rt: "
            content += str(self.rt_tickets.all()[0].ticket_id)
        content += "\n"
        content += "reimbursement: "
        content += '"' + self.get_fully_qualified_url() + '"'
        content += "\n"
        content += "invoice: "
        content += '"' + self.get_fully_qualified_url() + '/pdf"'
        content += "\n"
        content += "payment_amount: "
        total = self.total
        content += f"{total[0]} {total[1]}"
        content += "\n"
        for t, key in self.summarize_by_beancount_type().items():
            content += f"{key.name}\t{key.requested} USD\n"
            if key.name == "Liabilities:Payable":
                content += ' tax_implication: "Reimbursement"\n'
            else:
                content += f' project: "{self.approver_group}"\n'
        return content

    class Meta:
        ordering = ["-id"]
