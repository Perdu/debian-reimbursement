import hashlib
from contextlib import ExitStack
from decimal import Decimal

from django.conf import settings
from django.db import models

from image_wrangler.client import ThumbnailerClient
from reimbursements.storage import (
    generate_receipt_filename,
    receipts_storage,
)
from reimbursements.models.request import Request


class Receipt(models.Model):
    request = models.ForeignKey(
        Request, on_delete=models.CASCADE, related_name="receipts"
    )
    file = models.FileField(
        storage=receipts_storage, upload_to=generate_receipt_filename
    )
    file_sha3_256 = models.BinaryField(max_length=32, db_index=True)
    description = models.TextField()
    custom_conversion = models.BooleanField(default=False)

    @property
    def subject(self):
        lines = self.description.splitlines()
        if lines:
            return lines[0].strip()
        return ""

    @property
    def total(self):
        return (
            self.lines.aggregate(total=models.Sum("converted_amount"))["total"]
            or Decimal(0),
            self.request.currency,
        )

    def _iter_thumbnailers(self):
        for max_dimensions, output_type in settings.RECEIPTS_THUMBNAILS:
            yield ThumbnailerClient(self.file, max_dimensions, output_type)

    @property
    def thumbnail(self):
        return receipts_storage.open(next(self._iter_thumbnailers()).thumbnail_path())

    def generate_thumbnails(self):
        for thumbnailer in self._iter_thumbnailers():
            thumbnailer.generate_thumbnail()

    def set_file_hash(self):
        hash_ = hashlib.sha3_256()
        with ExitStack() as stack:
            if self.file.closed:
                f = stack.enter_context(self.file.open("rb"))
            else:
                # In the initial upload, we don't want to close the temporary
                # file
                f = self.file
                f.seek(0)

            while block := f.read(hash_.block_size):
                hash_.update(block)

        self.file_sha3_256 = hash_.digest()

    def save(self, *args, **kwargs):
        self.set_file_hash()
        super().save(*args, **kwargs)

    def __str__(self):
        return f"{self.request}: {self.description}"

    class Meta:
        ordering = ["id"]
        constraints = [
            models.UniqueConstraint(
                fields=["request", "file_sha3_256"], name="unique_receipts"
            ),
        ]
