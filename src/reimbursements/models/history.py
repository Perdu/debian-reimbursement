from decimal import Decimal

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

from reimbursements.models.types import ExpenseTypeAllocation

User = get_user_model()


class RequestHistory(models.Model):
    class Events(models.TextChoices):
        CREATED = "created", _("Created")
        UPDATED = "updated", _("Updated")
        PROPOSED = "proposed", _("Proposed")
        APPROVED = "approved", _("Approved")
        REJECTED = "rejected", _("Rejected")
        MOREINFO_APPROVAL = (
            "moreinfo_approval",
            _("Information requested for approval"),
        )
        WITHDRAWN = "withdrawn", _("Withdrawn")
        SUBMITTED = "submitted", _("Submitted")
        REIMBURSED = "reimbursed", _("Reimbursed")
        MOREINFO_REIMBURSAL = (
            "moreinfo_reimbursal",
            _("Information requested for reimbursement"),
        )
        NOTE = "note", _("Note")
        RECEIPT_ADDED = "receipt_added", _("Receipt Added")
        RECEIPT_DELETED = "receipt_deleted", _("Receipt Deleted")
        RECEIPT_UPDATED = "receipt_updated", _("Receipt Updated")
        BUDGET_INCREASE = "budget_increase", _("Returned for budget increase")
        CURRENCY_CHANGE = "currency_change", _("Changed request currency")
        PAYER_CHANGE = "payer_change", _("Changed request payer")
        TRANSFER_INITIATED = "transfer_initiated", _("Generated a transfer token")
        TRANSFERRED = "transferred", _("Transferred request")

    request = models.ForeignKey(
        "Request", on_delete=models.CASCADE, related_name="history"
    )
    actor = models.ForeignKey(User, on_delete=models.PROTECT)
    timestamp = models.DateTimeField(auto_now_add=True)
    event = models.CharField(max_length=20, choices=Events.choices)
    budget = models.JSONField(blank=True, null=True)
    details = models.TextField()

    class Meta:
        ordering = ["timestamp"]
        verbose_name = _("Request history event")
        verbose_name_plural = _("Request history")

    def __str__(self):
        return f"{self.timestamp} {self.actor.username} {self.event}"

    def budget_total_qualified(self):
        if self.budget:
            total = self.budget["Total"]
            return (Decimal(total["requested"]), total["currency"])

    def budget_allocation(self):
        for name, line in sorted(self.budget.items()):
            if name == "Total":
                continue
            yield ExpenseTypeAllocation(name=name, **line)
        yield ExpenseTypeAllocation(name="Total", **self.budget["Total"], special=True)
