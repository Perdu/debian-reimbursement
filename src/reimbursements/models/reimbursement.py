from django.conf import settings
from django.db import models

from reimbursements.models.types import ExpenseType


class ReimbursementLine(models.Model):
    request = models.ForeignKey(
        "Request", on_delete=models.CASCADE, related_name="reimbursement_lines"
    )
    expense_type = models.ForeignKey(ExpenseType, on_delete=models.PROTECT)
    description = models.TextField(blank=True)
    amount = models.DecimalField(decimal_places=2, max_digits=15)

    @property
    def qualified_amount(self):
        return (self.amount, self.request.currency)


class ReimbursementCost(models.Model):
    request = models.ForeignKey(
        "Request", on_delete=models.CASCADE, related_name="reimbursement_cost"
    )
    description = models.TextField()
    amount = models.DecimalField(decimal_places=2, max_digits=15)
    currency = models.CharField(max_length=3, default=settings.DEFAULT_CURRENCY)

    @property
    def qualified_amount(self):
        return (self.amount, self.request.currency)
