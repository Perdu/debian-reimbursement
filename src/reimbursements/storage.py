import os
from pathlib import Path

from django.conf import settings
from django.core.checks import Error, Tags, register
from django.core.files.storage import FileSystemStorage


class PrivateStorage(FileSystemStorage):
    def url(self, name):
        # FIXME: Ideally: base_url = None
        # But this breaks widget rendering.
        # So, instead we just return a useless URL.
        return "#"


class ReceiptsStorage(PrivateStorage):
    """Storage for receipts and their thumbnails"""

    def delete(self, name):
        if "/" not in name:
            for max_dimensions, output_type in settings.RECEIPTS_THUMBNAILS:
                super().delete(self.thumbnail_path(name, max_dimensions, output_type))
        return super().delete(name)

    def thumbnail_path(
        self, name: str, max_dimensions: tuple[int, int], output_type: str
    ) -> str:
        input_path = Path(name)
        width = int(max_dimensions[0])
        height = int(max_dimensions[1])
        ext = output_type.lower()
        return f"thumbnails/{width}x{height}/{input_path.stem}.{ext}"


def generate_receipt_filename(instance, filename):
    return settings.RECEIPTS_FILENAME.format(instance=instance, filename=filename)


def generate_bundle_filename(instance):
    return settings.BUNDLE_FILENAME.format(instance=instance)


receipts_storage = ReceiptsStorage(location=settings.RECEIPTS_PATH)


# Not the correct tag, but the options are limited...
@register(Tags.sites, deploy=True)
def check_storage_is_writeable(app_configs, **kwargs):
    errors = []
    storage_path = receipts_storage.path("")
    if not os.access(storage_path, os.W_OK):
        errors.append(
            Error(
                "receipts directory not writeable",
                hint="Make the RECEIPTS_PATH a directory that the Django user can write to",
                id="reimbursements.E001",
            )
        )

    return errors
