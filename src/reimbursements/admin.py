from django.contrib import admin
from django.db import models
from django.utils.translation import gettext_lazy as _

from django_json_widget.widgets import JSONEditorWidget
from historical_currencies.formatting import render_amount

from reimbursements.models import (
    ExpenseType,
    Profile,
    Receipt,
    ReceiptLine,
    ReimbursementCost,
    ReimbursementLine,
    Request,
    RequestHistory,
    RequestLine,
    RequestType,
    RTTicket,
)


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ("user",)
    formfield_overrides = {
        models.JSONField: {"widget": JSONEditorWidget},
    }


@admin.register(RequestType)
class RequestTypeAdmin(admin.ModelAdmin):
    list_display = ("name", "deleted")

    def get_queryset(self, request):
        return RequestType.all_objects.all()


@admin.register(ExpenseType)
class ExpenseTypeAdmin(admin.ModelAdmin):
    list_display = ("name", "request_types_summary", "deleted")

    def request_types_summary(self, request):
        types = request.request_types.all()
        summary = [str(t) for t in types[:3]]
        if len(summary) == 3 and types.count() > 3:
            summary.append("...")
        return ", ".join(summary)

    def get_queryset(self, request):
        return ExpenseType.all_objects.all()


@admin.register(RequestHistory)
class RequestHistoryAdmin(admin.ModelAdmin):
    pass


class RequestLineInline(admin.TabularInline):
    model = RequestLine
    extra = 1


class ReceiptInline(admin.TabularInline):
    """
    Show a minimal record of a receipt inline.

    Defer to ReceiptAdmin for editing.
    """

    model = Receipt
    fields = ["description", "total"]
    readonly_fields = ["description", "total"]
    extra = 0
    show_change_link = True
    can_delete = False

    def total(self, receipt):
        return render_amount(*receipt.total)


class RequestHistoryInline(admin.TabularInline):
    """
    Show a read-only view of history.

    Defer to RequestHistoryAdmin for editing.
    """

    model = RequestHistory
    # FIXME: Display budget concisely instead of hiding it.
    fields = ["timestamp", "actor", "event", "details"]
    readonly_fields = ["timestamp", "actor", "event", "budget", "details"]
    extra = 0
    show_change_link = True
    can_delete = False


class ReimbursementLineInline(admin.TabularInline):
    model = ReimbursementLine
    extra = 0


class ReimbursementCostInline(admin.TabularInline):
    model = ReimbursementCost
    extra = 0


class RTTicketInline(admin.TabularInline):
    model = RTTicket
    extra = 0


class ClaimedListFilter(admin.SimpleListFilter):
    title = _("claimed")
    parameter_name = "claimed"

    def lookups(self, request, model_admin):
        return [
            ("1", _("Yes")),
            ("0", _("No")),
        ]

    def queryset(self, request, queryset):
        if self.value() == "1":
            return queryset.filter(requester__isnull=False)
        elif self.value() == "0":
            return queryset.filter(requester__isnull=True)


@admin.register(Request)
class RequestAdmin(admin.ModelAdmin):
    inlines = [
        RequestLineInline,
        ReceiptInline,
        RequestHistoryInline,
        ReimbursementLineInline,
        ReimbursementCostInline,
        RTTicketInline,
    ]
    search_fields = [
        "id",
        "description",
        "requester__email",
        "requester__username",
        "requester__first_name",
        "requester__last_name",
    ]
    list_display = [
        "__str__",
        "state",
        "requester",
        "approver_group",
        "payer_group",
    ]
    list_filter = [
        "state",
        "approver_group",
        "payer_group",
        ClaimedListFilter,
    ]
    formfield_overrides = {
        models.JSONField: {"widget": JSONEditorWidget},
    }


class ReceiptLineInline(admin.TabularInline):
    model = ReceiptLine
    readonly_fields = ["converted_amount"]
    extra = 1

    def converted_amount(self, receipt_line):
        return render_amount(*receipt_line.qualified_converted_amount)


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    inlines = [ReceiptLineInline]
