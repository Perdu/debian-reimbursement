from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils import timezone

from reimbursements.email import send_email
from reimbursements.models import Request


class Command(BaseCommand):
    help = "Send reminder emails about open requests"

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            action="store_true",
            help="Just show the people we'll remind, don't remind them.",
        )

    def handle(self, *args, **options):
        threshold = timezone.now() - timedelta(days=14)
        for request in (
            Request.objects.filter(
                state__in=(Request.States.DRAFT, Request.States.APPROVED),
                requester__isnull=False,
            )
            .exclude(
                state=Request.States.APPROVED,
                expected_date__gt=threshold.date(),
            )
            .exclude(history__timestamp__gte=threshold)
        ):
            self.ping(request, options["dry_run"])

    def ping(self, request, dry_run=False):
        if dry_run:
            print(f"Would badger {request.requester.get_full_name()}")
            return
        send_email(
            "reimbursements/email/open_request_reminder.txt",
            {"object": request},
            [request.requester],
        )
