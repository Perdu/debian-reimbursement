from django.core.management.base import BaseCommand

from reimbursements.models import Receipt


class Command(BaseCommand):
    help = "Regenerate receipt thumbnails"

    def add_arguments(self, parser):
        parser.add_argument(
            "--request",
            type=int,
            metavar="ID",
            help="Request to re-generate thumbnails for. Default, all requests",
        )

    def handle(self, *args, **options):
        receipts = Receipt.objects

        if options["request"]:
            receipts = receipts.filter(request_id=options["request"])
        else:
            receipts = receipts.all()

        for receipt in receipts:
            print(f"Request {receipt.request.id} Receipt {receipt.id}")
            receipt.generate_thumbnails()
