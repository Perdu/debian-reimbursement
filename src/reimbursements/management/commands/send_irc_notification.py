from django.core.management.base import BaseCommand

from reimbursements.integrations.irc import notify


class Command(BaseCommand):
    help = "Send a test IRC notification"

    def add_arguments(self, parser):
        parser.add_argument(
            "message",
            metavar="MSG",
            help="Message to send",
        )

    def handle(self, *args, **options):
        notify(options["message"])
