from datetime import datetime
from decimal import Decimal

from django.contrib.auth import get_user_model
from django.test import SimpleTestCase

from reimbursements.models.history import RequestHistory
from reimbursements.models.types import ExpenseTypeAllocation


class TestRequestHistory(SimpleTestCase):
    def setUp(self):
        self.budget = {
            "Category A": {
                "requested": 12,
                "currency": "USD",
            },
            "Total": {
                "requested": 37,
                "currency": "USD",
            },
            "Z Category B": {
                "requested": 25,
                "currency": "USD",
            },
        }

    def test_str(self):
        User = get_user_model()
        line = RequestHistory(
            actor=User(username="actor"),
            timestamp=datetime(2023, 1, 21, 13, 33, 47),
            event=RequestHistory.Events.CREATED,
        )
        self.assertEqual(str(line), "2023-01-21 13:33:47 actor created")

    def test_budget_total_qualified(self):
        line = RequestHistory(budget=self.budget)
        self.assertEqual(line.budget_total_qualified(), (Decimal(37), "USD"))

    def test_budget_total_qualified_unspecified(self):
        line = RequestHistory()
        self.assertIsNone(line.budget_total_qualified())

    def test_budget_allocation(self):
        line = RequestHistory(budget=self.budget)

        allocation = list(line.budget_allocation())

        self.assertEqual(len(allocation), 3)
        self.assertIsInstance(allocation[0], ExpenseTypeAllocation)

        self.assertEqual(allocation[0].name, "Category A")
        self.assertEqual(allocation[0].requested, Decimal(12))
        self.assertEqual(allocation[0].currency, "USD")

        self.assertEqual(allocation[1].name, "Z Category B")
        self.assertEqual(allocation[1].requested, Decimal(25))
        self.assertEqual(allocation[1].currency, "USD")

        self.assertEqual(allocation[2].name, "Total")
        self.assertEqual(allocation[2].requested, Decimal(37))
        self.assertEqual(allocation[2].currency, "USD")
