from decimal import Decimal

from django.core.exceptions import ValidationError
from django.test import SimpleTestCase, TestCase

from reimbursements.models.request import Request
from reimbursements.models.requestline import RequestLine
from reimbursements.models.types import ExpenseType

from reimbursements.tests.helpers import RequestHelpers


class TestRequestLineSimpleMethods(SimpleTestCase):
    def test_qualified_amount(self):
        line = RequestLine(request=Request(currency="EUR"), amount=Decimal(100))
        self.assertEqual(line.qualified_amount, (Decimal(100), "EUR"))


class TestRequestLine(TestCase, RequestHelpers):
    def test_clean(self):
        self.create_request()
        line = self.create_request_line()

        self.assertIsNone(line.clean())

    def test_clean_rejects_invalid_expense_type(self):
        self.create_request()
        unrelated = ExpenseType.objects.create(name="Unrelated")
        line = self.create_request_line(expense_type=unrelated)

        with self.assertRaises(ValidationError) as cm:
            line.clean()

        self.assertIn("expense_type", cm.exception.error_dict)
        self.assertEqual(len(cm.exception.error_dict["expense_type"]), 1)
        inner_error = cm.exception.error_dict["expense_type"][0]
        self.assertEqual(
            inner_error.messages,
            ["Unrelated is not a valid expense type for Travel requests"],
        )
