from decimal import Decimal

from django.test import SimpleTestCase, TestCase

from reimbursements.models.types import ExpenseType, ExpenseTypeAllocation, RequestType


class TestRequestTypeSimpleMethods(SimpleTestCase):
    def test_str(self):
        request_type = RequestType(name="Testing")
        self.assertEqual(str(request_type), "Testing")


class TestRequestType(TestCase):
    def test_expense_type_map(self):
        travel = RequestType.objects.create(name="Travel")
        hardware = RequestType.objects.create(name="Hardware")
        accomm = ExpenseType.objects.create(name="Accommodation")
        accomm.request_types.set([travel])
        other = ExpenseType.objects.create(name="Other")
        other.request_types.set([travel, hardware])
        self.assertEqual(
            RequestType.expense_type_map(),
            {
                accomm.id: [travel.id, other.id],
                hardware.id: [other.id],
            },
        )


class TestExpenseType(SimpleTestCase):
    def test_str(self):
        expense_type = ExpenseType(name="Testing")
        self.assertEqual(str(expense_type), "Testing")


class TestExpenseTypeAllocation(SimpleTestCase):
    def setUp(self):
        self.eta = ExpenseTypeAllocation(
            name="Testing",
            currency="USD",
            requested=100,
            spent=75,
            reimbursed=0,
            special=False,
        )

    def test_minimal_constructor(self):
        eta = ExpenseTypeAllocation(
            name="Testing",
            currency="USD",
        )
        self.assertEqual(eta.requested, Decimal(0))
        self.assertEqual(eta.spent, Decimal(0))
        self.assertIs(eta.special, False)

    def test_qualified_requested(self):
        self.assertEqual(self.eta.qualified_requested, (Decimal(100), "USD"))

    def test_qualified_spent(self):
        self.assertEqual(self.eta.qualified_spent, (Decimal(75), "USD"))

    def test_qualified_reimbursed(self):
        self.assertEqual(self.eta.qualified_reimbursed, (Decimal(0), "USD"))

    def test_remaining(self):
        self.assertEqual(self.eta.remaining, Decimal(25))

    def test_qualified_remaining(self):
        self.assertEqual(self.eta.qualified_remaining, (Decimal(25), "USD"))

    def test_over_budget(self):
        self.assertFalse(self.eta.over_budget)

    def test_within_budget(self):
        self.assertTrue(self.eta.within_budget)

    def test_approved(self):
        self.assertTrue(self.eta.approved)

    def test_reimburseable(self):
        self.assertEqual(self.eta.reimburseable, Decimal(75))

    def test_qualified_approved(self):
        self.assertEqual(self.eta.qualified_approved, (Decimal(100), "USD"))

    def test_qualified_reimburseable(self):
        self.assertEqual(self.eta.qualified_reimburseable, (Decimal(75), "USD"))

    def test_to_dict(self):
        self.assertEqual(
            self.eta.to_dict(),
            {
                "currency": "USD",
                "requested": 100.0,
                "spent": 75.0,
                "reimbursed": 0.0,
            },
        )

    def test_spent_over_budget(self):
        self.eta.spent = Decimal(115)
        self.assertEqual(self.eta.remaining, Decimal(-15))
        self.assertTrue(self.eta.over_budget)
        self.assertFalse(self.eta.within_budget)
        with self.settings(OVERBUDGET_LEEWAY_PERCENTAGE=5):
            self.assertEqual(self.eta.approved, Decimal(105))
