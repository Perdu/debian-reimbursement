from datetime import date
from decimal import Decimal
from io import BytesIO
from random import randrange

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.files import File

from historical_currencies.exchange import exchange
from historical_currencies.models import ExchangeRate
from PIL import Image

from reimbursements.models import (
    ExpenseType,
    Receipt,
    ReceiptLine,
    ReimbursementLine,
    Request,
    RequestHistory,
    RequestLine,
    RequestType,
)


class RequestHelpers:
    def create_basics(self):
        # FIXME: Make this more explicitly opt-in
        User = get_user_model()
        if User.objects.filter(username="user").exists():
            return
        self.user = User.objects.create_user(username="user")
        self.approvers = Group.objects.create(name="approvers")
        self.payers = Group.objects.create(name="payers")
        self.travel = RequestType.objects.create(name="Travel")
        self.accomm = ExpenseType.objects.create(name="Accommodation")
        self.accomm.request_types.set([self.travel])

    def create_request(self):
        self.create_basics()
        self.request = Request.objects.create(
            requester=self.user,
            request_type=self.travel,
            description="A Test Request",
            currency="USD",
            approver_group=self.approvers,
            payer_group=self.payers,
        )
        RequestHistory.objects.create(
            request=self.request,
            actor=self.user,
            event=RequestHistory.Events.CREATED,
        )
        return self.request

    def create_request_line(self, expense_type=None):
        if expense_type is None:
            expense_type = self.accomm

        return RequestLine.objects.create(
            request=self.request,
            expense_type=expense_type,
            description="Accommodation",
            amount=Decimal(100),
        )

    def create_receipt_image(self, random=True):
        output = BytesIO()
        with Image.new("RGB", (100, 100), "white") as image:
            if random:
                for i in range(10):
                    image.putpixel(
                        (randrange(image.width), randrange(image.height)),
                        (randrange(256), randrange(256), randrange(256)),
                    )
            image.save(output, "PNG", compresslevel=0, optimize=False)
        output.seek(0)
        return File(output, name="test_receipt.png")

    def create_receipt(self, custom_conversion=False, random_image=True):
        receipt = Receipt.objects.create(
            request=self.request,
            file=self.create_receipt_image(random=random_image),
            description="A Test Receipt",
            custom_conversion=custom_conversion,
        )
        self.addCleanup(receipt.file.delete, save=False)
        return receipt

    def create_exchange_rate(self):
        ExchangeRate.objects.update_or_create(
            date=date(2023, 1, 21),
            base_currency="USD",
            currency="EUR",
            rate=Decimal("0.91934"),
        )

    def create_receipt_line(
        self, receipt, amount=Decimal(59), currency="EUR", expense_type=None
    ):
        if expense_type is None:
            expense_type = self.accomm

        converted_amount = exchange(
            amount, currency, self.request.currency, date(2023, 1, 21)
        )

        return ReceiptLine.objects.create(
            receipt=receipt,
            date=date(2023, 1, 21),
            description="Test Hotel",
            amount=amount,
            converted_amount=converted_amount,
            currency=currency,
            expense_type=expense_type,
        )

    def create_reimbursement(self, expense_type=None, amount=Decimal("64.18")):
        if expense_type is None:
            expense_type = self.accomm

        return ReimbursementLine.objects.create(
            request=self.request,
            expense_type=expense_type,
            description="Reimbursed",
            amount=amount,
        )
