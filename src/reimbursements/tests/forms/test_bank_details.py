from django.test import SimpleTestCase

from reimbursements.forms.bank_details import (
    ACHBankDetailsForm,
    SWIFTBankDetailsForm,
    label_bank_details,
    select_form,
)


class TestFormRegistration(SimpleTestCase):
    def test_select_by_wise_type(self):
        self.assertEqual(select_form(wise_type="swift"), SWIFTBankDetailsForm)

    def test_select_unknown_wise_type(self):
        self.assertIsNone(select_form(wise_type="foo"))

    def test_wise_type_is_set(self):
        self.assertEqual(SWIFTBankDetailsForm.wise_type, "swift")

    def test_select_by_currency(self):
        self.assertEqual(select_form(currency="USD"), ACHBankDetailsForm)

    def test_select_unknown_currency(self):
        self.assertIsNone(select_form(currency="ZAR"))


class TestDataRender(SimpleTestCase):
    def test_render_passthrough(self):
        bank_details = {
            "wise_type": "foo",
            "iban": "BE68539007547034",
            "recipient_name": "Foo Bar",
        }
        self.assertEqual(label_bank_details(bank_details), bank_details)

    def test_render_sepa(self):
        bank_details = {
            "wise_type": "iban",
            "iban": "BE68539007547034",
            "recipient_name": "Foo Bar",
        }
        expected = {
            "Transfer Method": "European SEPA Transfers",
            "Name": "Foo Bar",
            "IBAN": "BE68539007547034",
            "SWIFT/BIC Code": None,
        }
        rendered = label_bank_details(bank_details)
        self.assertEqual(rendered, expected)
        # Check field ordering
        self.assertEqual(list(rendered.keys()), list(expected.keys()))
