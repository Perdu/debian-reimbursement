{% load i18n %}
{% load currency_format %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request {{ subject }} approved
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with subject=object.subject requester_name=object.requester_name total=object.total|currency %}
Dear {{ requester_name }},

Your request for {{ total }} for {{ subject }}
has been approved.
{% endblocktrans %}
{% if details %}{% blocktrans %}Approval note: {{ details }}{% endblocktrans %}{% endif %}
{% blocktrans with request_url=object.get_fully_qualified_url approver_group=object.approver_group.name %}
You may now spend up to this amount.
Please keep receipts, scan them, and upload them to the request.

{{ request_url }}

When you're done, submit the receipts and you will be reimbursed for your
expenses.

Thank you,

The {{ SITE_NAME }} system on behalf of {{ approver_group }}.
{% endblocktrans %}
{% endblock %}
