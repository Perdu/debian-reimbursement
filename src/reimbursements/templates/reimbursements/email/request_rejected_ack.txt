{% load i18n %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request rejected: {{ subject }}
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with approver_group=object.approver_group.name subject=object.subject requester_name=object.requester_name actor_name=actor.get_full_name|default:actor.username %}
Dear {{ approver_group }} member,

The request submitted by {{ requester_name }} for {{ subject }}
has been rejected by {{ actor_name }}.

{% endblocktrans %}
{% if details %}{% blocktrans %}Rejection note: {{ details }}{% endblocktrans %}{% endif %}

{{ object.get_fully_qualified_url }}
{% endblock %}
