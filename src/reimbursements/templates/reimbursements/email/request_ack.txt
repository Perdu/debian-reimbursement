{% load i18n %}
{% load currency_format %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request {{ subject }} received
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with subject=object.subject requester_name=object.requester_name total=object.total|currency approver_group=object.approver_group.name %}
Dear {{ requester_name }},

Your request for {{ total }} for {{ subject }}
has been received and will be reviewed.

You will be notified by email, when this has been completed.

Thank you,

The {{ SITE_NAME }} system on behalf of {{ approver_group }}.
{% endblocktrans %}
{% endblock %}
