{% load i18n %}
{% load currency_format %}
{% block subject %}
{% blocktrans with subject=object.subject state=object.state %}
{{ SITE_NAME }}: {{ state|title }} Request {{ subject }} requires action
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with subject=object.subject requester_name=object.requester_name total=object.total|currency state=object.state|title time=object.updated|timesince %}
Dear {{ requester_name }},

Your request for reimbursement for {{ total }} for {{ subject }}
has been left in the {{ state }} state for {{ time }} without any action.
If you no longer plan to claim this request, please delete it.
If you still need more time to prepare the request, that's probably OK. But
please try to get it done soon.

The next step for your request is:
{% endblocktrans %}{% spaceless %}
{% if object.state == "draft" %}
{% trans "Submit your draft request for approval." %}
{% elif object.state == "approved" %}
{% trans "Add receipts. When they're all added, submit your request for reimbursement." %}
{% endif %}
{% endspaceless %}{% blocktrans with url=object.get_fully_qualified_url %}
{{ url }}

Thank you,

The {{ SITE_NAME }} system.
{% endblocktrans %}
{% endblock %}
