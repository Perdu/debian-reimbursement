{% load i18n %}
{% load currency_format %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request received: {{ subject }}
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with approver_group=object.approver_group.name requester_name=object.requester_name request_url=object.get_fully_qualified_url subject=object.subject total=object.total|currency %}
Dear {{ approver_group }} member,

I have submitted a request for {{ total }} for {{ subject }}.
You can review the full request here:
{{ request_url }}

Thank you,

The {{ SITE_NAME }} system on behalf of {{ requester_name }}.
{% endblocktrans %}
{% endblock %}
