from django.urls import path

from reimbursements.views.bank_details import (
    BankDetailsView,
    BankFormSelectorView,
)
from reimbursements.views.lifecycle import (
    RequestCommentView,
    RequestDeleteView,
    RequestReimburseReturnView,
    RequestReimburseView,
    RequestReviewView,
    RequestSubmitDraftView,
    RequestSubmitIncreaseView,
    RequestSubmitView,
)
from reimbursements.views.receipt import (
    ReceiptCreateView,
    ReceiptDeleteView,
    ReceiptEditView,
    ReceiptFileView,
    ReceiptThumbnailView,
    RequestReceiptsView,
)
from reimbursements.views.request import (
    RequestCreateView,
    RequestCurrencyChangeView,
    RequestEditView,
    RequestPDFBundleView,
    RequestBeancountView,
    RequestPayerChangeView,
    RequestView,
)
from reimbursements.views.transfer import (
    RequestTransferInitiateView,
    RequestTransferClaimView,
)

urlpatterns = [
    path("new", RequestCreateView.as_view(), name="request-new"),
    path("<int:pk>", RequestView.as_view(), name="request"),
    path("<int:pk>/pdf", RequestPDFBundleView.as_view(), name="request-pdf"),
    path("<int:pk>/beancount", RequestBeancountView.as_view(), name="request-beancount"),
    path("<int:pk>/edit", RequestEditView.as_view(), name="request-edit"),
    path(
        "<int:pk>/submit-draft",
        RequestSubmitDraftView.as_view(),
        name="request-submit-draft",
    ),
    path("<int:pk>/review", RequestReviewView.as_view(), name="request-review"),
    path(
        "<int:pk>/bank-details",
        BankFormSelectorView.as_view(),
        name="bank-form-selector",
    ),
    path(
        "<int:pk>/bank-details/<str:wise_type>",
        BankDetailsView.as_view(),
        name="bank-details",
    ),
    path("<int:pk>/submit", RequestSubmitView.as_view(), name="request-submit"),
    path("<int:pk>/delete", RequestDeleteView.as_view(), name="request-delete"),
    path(
        "<int:pk>/reimburse", RequestReimburseView.as_view(), name="request-reimburse"
    ),
    path(
        "<int:pk>/reimburse-return",
        RequestReimburseReturnView.as_view(),
        name="request-reimburse-return",
    ),
    path(
        "<int:pk>/request-increase",
        RequestSubmitIncreaseView.as_view(),
        name="request-submit-increase",
    ),
    path(
        "<int:pk>/request-comment",
        RequestCommentView.as_view(),
        name="request-comment",
    ),
    path(
        "<int:pk>/change-currency",
        RequestCurrencyChangeView.as_view(),
        name="request-change-currency",
    ),
    path(
        "<int:pk>/change-payer",
        RequestPayerChangeView.as_view(),
        name="request-change-payer",
    ),
    path("<int:pk>/receipts", RequestReceiptsView.as_view(), name="request-receipts"),
    path(
        "<int:request_id>/receipts/new", ReceiptCreateView.as_view(), name="add-receipt"
    ),
    path(
        "<int:request_id>/receipts/<int:pk>/delete",
        ReceiptDeleteView.as_view(),
        name="delete-receipt",
    ),
    path(
        "<int:request_id>/receipts/<int:pk>/edit",
        ReceiptEditView.as_view(),
        name="edit-receipt",
    ),
    path(
        "<int:request_id>/receipts/<int:pk>/thumbnail",
        ReceiptThumbnailView.as_view(),
        name="view-receipt-thumbnail",
    ),
    path(
        "<int:request_id>/receipts/<int:pk>/view-file",
        ReceiptFileView.as_view(),
        name="view-receipt-file",
    ),
    path(
        "<int:pk>/transfer",
        RequestTransferInitiateView.as_view(),
        name="request-transfer-initiate",
    ),
    path(
        "transfer/<str:token>",
        RequestTransferClaimView.as_view(),
        name="request-transfer-claim",
    ),
]
