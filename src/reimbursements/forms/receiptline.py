from datetime import date

from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.functional import keep_lazy_text
from django.utils.translation import gettext_lazy as _

from crispy_forms.helper import FormHelper
from historical_currencies.choices import currency_choices
from historical_currencies.exceptions import ExchangeRateUnavailable
from historical_currencies.exchange import exchange, latest_rate

from reimbursements.models import Receipt, ReceiptLine
from reimbursements.widgets import NativeDateInput


class ReceiptLineForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["converted_amount"].required = False
        self.fields["converted_amount"].label = self.lazy_substitute_currency(
            _("Amount converted to {currency}")
        )

    @keep_lazy_text
    def lazy_substitute_currency(self, msg):
        return msg.format(currency=self.request_currency)

    @property
    def request_currency(self):
        # Validation failure flows:
        if not hasattr(self.instance, "receipt") or not hasattr(
            self.instance.receipt, "request"
        ):
            return self.initial["currency"]
        try:
            return self.instance.receipt.request.currency
        except Receipt.DoesNotExist:
            return self.initial["currency"]

    def clean_currency(self):
        currency = self.cleaned_data["currency"]
        date = self.cleaned_data.get("date")
        if not date:
            # Don't fail the validation, let the date error be shown
            return currency
        if currency == self.request_currency:
            return currency
        try:
            latest_rate(currency, self.request_currency, date)
        except ExchangeRateUnavailable:
            raise ValidationError(
                _(
                    "No exchange rate available for %(currency_from)s "
                    "to %(currency_to)s for %(date)s. Please contact "
                    "an admin to get it imported."
                )
                % {
                    "currency_from": currency,
                    "currency_to": self.request_currency,
                    "date": date,
                }
            )
        return currency

    def clean_converted_amount(self):
        converted_amount = self.cleaned_data["converted_amount"]
        amount = self.cleaned_data.get("amount")
        date = self.cleaned_data.get("date")
        if not (amount and date):
            # Let the other required fields show errors
            return converted_amount

        # No exchange rate will lead to currency failing to clean, so it's None
        currency = self.cleaned_data.get("currency")

        receipt = self.cleaned_data["receipt"]
        custom_conversion = receipt.custom_conversion

        if (converted_amount is None or not custom_conversion) and currency is not None:
            converted_amount = exchange(amount, currency, self.request_currency, date)
        return converted_amount

    class Meta:
        model = ReceiptLine
        widgets = {
            "currency": forms.Select(choices=currency_choices()),
            "description": forms.TextInput(),
            "date": NativeDateInput(
                attrs={
                    "max": date.today,
                }
            ),
        }
        exclude = ["request"]


class ReceiptLineFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form_tag = False
        self.template = f"{settings.CRISPY_TEMPLATE_PACK}/table_inline_formset.html"


class ReceiptLineInlineFormset(forms.models.BaseInlineFormSet):
    """
    Formset with a default currency configured through a default_currency attribute
    """

    def get_form_kwargs(self, index):
        kwargs = super().get_form_kwargs(index)
        is_empty_form = index is None  # the default form copied by JS
        is_new_instance = self.instance.pk is None
        if is_empty_form or is_new_instance:
            kwargs.setdefault("initial", {}).setdefault(
                "currency", self.default_currency
            )
        return kwargs

    def clean(self):
        count = sum(
            1
            for form in self.forms
            if getattr(form, "cleaned_data", None)
            and not form.cleaned_data.get("DELETE", False)
        )
        if count < 1:
            raise ValidationError(_("At least one receipt line is required."))
        return super().clean()


ReceiptLineFormset = forms.models.inlineformset_factory(
    Receipt,
    ReceiptLine,
    extra=1,
    form=ReceiptLineForm,
    formset=ReceiptLineInlineFormset,
)
ReceiptLineEditFormset = forms.models.inlineformset_factory(
    Receipt,
    ReceiptLine,
    extra=0,
    form=ReceiptLineForm,
    formset=ReceiptLineInlineFormset,
)
