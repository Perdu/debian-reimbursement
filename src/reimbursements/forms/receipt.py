import hashlib

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from crispy_forms.helper import FormHelper

from image_wrangler.client import UnknownFileTypeException, ValidatorClient
from reimbursements.models import Receipt, Request


class ReceiptForm(forms.ModelForm):
    # Stashed on the form, in the view
    request: Request | None = None

    class Meta:
        model = Receipt
        fields = ("file", "description", "custom_conversion")
        help_texts = {
            "file": _("Supported formats: PDF, JPEG, PNG, WEBP."),
            "description": _(
                "Describe the receipt in a sentence or two. "
                "First line is the subject (like a git commit)."
            ),
            "custom_conversion": _(
                "The amount you need to be reimbursed may not be a simple "
                "mid-market conversion of the amount you spent. "
                "You may have other payment fees or conversion "
                "adjustments."
            ),
        }
        widgets = {
            "description": forms.Textarea(attrs={"rows": 3}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

    def clean_file(self):
        file = self.cleaned_data["file"]

        hash_ = hashlib.sha3_256()
        while block := file.read(hash_.block_size):
            hash_.update(block)
        file.seek(0)

        if (
            Receipt.objects.filter(request=self.request, file_sha3_256=hash_.digest())
            .exclude(pk=self.instance.pk)
            .exists()
        ):
            raise ValidationError(
                _("This file is already used as a receipt on this request")
            )

        self.file_sha3_256 = hash_.digest()
        try:
            if not ValidatorClient(file).validate():
                raise ValidationError(_("Invalid receipt image"))
        except UnknownFileTypeException:
            raise ValidationError(_("Invalid receipt image"))
        return file

    def save(self, commit=True):
        instance = super().save(commit=False)
        if instance._state.adding:
            instance.request = self.request
        if commit:
            instance.save()
        return instance
