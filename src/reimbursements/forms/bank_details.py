from collections.abc import Callable, Iterable
from typing import Any

from django import forms
from django.core.exceptions import ValidationError
from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _
from django.utils.functional import Promise as StrPromise

from crispy_forms.helper import FormHelper
import stdnum.bic
import stdnum.br.cpf
import stdnum.exceptions
import stdnum.iban
import stdnum.us.rtn

from reimbursements.fields import RequiredCountries


def stdnum_validator(module) -> Callable[[Any], None]:
    """Turn an stdnum validator into a Django validator"""

    def validator(value: Any) -> None:
        try:
            module.validate(value)
        except stdnum.exceptions.ValidationError as e:
            raise ValidationError(e)

    return validator


class AccountTypeChoices(TextChoices):
    CHECKING = "checking", _("Checking")
    SAVINGS = "savings", _("Savings")


class BankDetailsForm(forms.Form):
    wise_type: str
    description: StrPromise | str

    def __init__(self, initial=None, instance=None, **kwargs):
        object_data = {}
        self.instance = instance
        if instance and instance.bank_details:
            object_data = instance.bank_details
        if initial is not None:
            object_data.update(initial)
        super().__init__(initial=object_data, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

    def full_clean(self):
        super().full_clean()
        if hasattr(self, "cleaned_data"):
            self.instance.bank_details = self.cleaned_data
            self.instance.bank_details["wise_type"] = self.wise_type

    def save(self, commit=True):
        if self.errors:
            raise ValueError("Could not save with errors")
        if commit:
            self.instance.save()
        return self.instance


BY_WISE_TYPE: dict[str, type[BankDetailsForm]] = {}
BY_CURRENCY: dict[str, type[BankDetailsForm]] = {}


def select_form(
    wise_type: str | None = None, currency: str | None = None
) -> type[BankDetailsForm] | None:
    if wise_type:
        if wise_type in BY_WISE_TYPE:
            return BY_WISE_TYPE[wise_type]
    if currency:
        if currency in BY_CURRENCY:
            return BY_CURRENCY[currency]
    return None


def register_bank_form(
    wise_type: str | None = None, currencies: Iterable[str] | None = None
):
    def decorator(cls):
        cls.wise_type = wise_type

        assert wise_type not in BY_WISE_TYPE
        BY_WISE_TYPE[wise_type] = cls

        if currencies:
            for currency in currencies:
                assert currency not in BY_CURRENCY
                BY_CURRENCY[currency] = cls

        return cls

    return decorator


def label_bank_details(bank_details: dict[str, str]) -> dict[str, str]:
    form = select_form(wise_type=bank_details.get("wise_type"))
    if not form:
        return bank_details
    bound_form = form(initial=bank_details)
    output = {
        str(_("Transfer Method")): str(form.description),
    }
    for bound_field in bound_form:
        output[str(bound_field.field.label)] = bound_field.value()
    return output


@register_bank_form(wise_type="aba", currencies=["USD"])
class ACHBankDetailsForm(BankDetailsForm):
    description = _("North American ACH Transfers")
    recipient_name = forms.CharField(label=_("Name"))
    accountNumber = forms.CharField(label=_("Account Number"))
    abartn = forms.CharField(
        label=_("Routing Number"), validators=[stdnum_validator(stdnum.us.rtn)]
    )
    accountType = forms.ChoiceField(
        label=_("Account Type"), choices=AccountTypeChoices.choices
    )
    address_firstLine = forms.CharField(label=_("Address Line 1"))
    address_secondLine = forms.CharField(label=_("Address Line 2"), required=False)
    address_city = forms.CharField(label=_("City"))
    address_state = forms.CharField(label=_("State"), required=False)
    address_postCode = forms.CharField(label=_("Postal Code"))
    address_country = forms.ChoiceField(label=_("Country"), choices=RequiredCountries())


@register_bank_form(wise_type="interac", currencies=["CAD"])
class InteracBankDetailsForm(BankDetailsForm):
    description = _("Canadian Interac Transfers")
    recipient_name = forms.CharField(label=_("Name"))
    email = forms.EmailField(label=_("Email"), help_text=_("Interac registered email"))


@register_bank_form(
    wise_type="iban",
    currencies=[
        "BGN",
        "CHF",
        "CZK",
        "DKK",
        "EUR",
        "GBP",
        "GEL",
        "HUF",
        "NOK",
        "PLN",
        "RON",
        "SEK",
        "TRY",
        "UAH",
    ],
)
class SEPABankDetailsForm(BankDetailsForm):
    description = _("European SEPA Transfers")
    recipient_name = forms.CharField(label=_("Name"))
    iban = forms.CharField(label=_("IBAN"), validators=[stdnum_validator(stdnum.iban)])
    bic = forms.CharField(
        label=_("SWIFT/BIC Code"),
        validators=[stdnum_validator(stdnum.bic)],
        help_text=_("(Optional) if known, it may make the transfer easier"),
        required=False,
    )


@register_bank_form(wise_type="swift")
class SWIFTBankDetailsForm(BankDetailsForm):
    description = _("International SWIFT Transfers")
    recipient_name = forms.CharField(label=_("Name"))
    accountNumber = forms.CharField(label=_("Account Number"), help_text=_("Or IBAN"))
    bic = forms.CharField(
        label=_("SWIFT/BIC Code"), validators=[stdnum_validator(stdnum.bic)]
    )
    address_firstLine = forms.CharField(label=_("Address Line 1"))
    address_secondLine = forms.CharField(label=_("Address Line 2"), required=False)
    address_city = forms.CharField(label=_("City"))
    address_state = forms.CharField(label=_("State"), required=False)
    address_postCode = forms.CharField(label=_("Postal Code"))
    address_country = forms.ChoiceField(label=_("Country"), choices=RequiredCountries())


@register_bank_form(wise_type="wise")
class WiseBankDetailsForm(BankDetailsForm):
    description = _("Wise Account")
    recipient_name = forms.CharField(label=_("Name"))
    wisetag = forms.CharField(label=_("Wisetag"), help_text=_("Username on wise.com"))


@register_bank_form(wise_type="brazil", currencies=["BRL"])
class BrazilBankDetailsForm(BankDetailsForm):
    description = _("Transfers to Brazil")
    recipient_name = forms.CharField(label=_("Name"))
    accountNumber = forms.CharField(label=_("Account Number"), help_text=_("Or IBAN"))
    bankCode = forms.CharField(
        label=_("Bank Code"), help_text=_("3-digit Brazilian Bank Code")
    )
    branchCode = forms.CharField(label=_("Branch Code"))
    accountType = forms.ChoiceField(
        label=_("Account Type"), choices=AccountTypeChoices.choices
    )
    cpf = forms.CharField(
        label=_("Tax registration number (CPF)"),
        validators=[stdnum_validator(stdnum.br.cpf)],
    )


@register_bank_form(wise_type="indian", currencies=["INR"])
class IndianBankDetailsForm(BankDetailsForm):
    description = _("Transfers to India")
    recipient_name = forms.CharField(label=_("Name"))
    accountNumber = forms.CharField(label=_("Account Number"))
    ifscCode = forms.CharField(label=_("IFSC Code"))


@register_bank_form(wise_type="south_korean_paygate", currencies=["KRW"])
class PayGateBankDetailsForm(BankDetailsForm):
    description = _("South Korean PayGate transfers")
    recipient_name = forms.CharField(label=_("Name"))
    dateOfBirth = forms.DateField(label=_("Birth Date"), input_formats=["%Y-%m-%d"])
    bank_name = forms.CharField(label=_("Bank Name"))
    accountNumber = forms.CharField(label=_("Account Number"))
    address_firstLine = forms.CharField(label=_("Address Line 1"))
    address_secondLine = forms.CharField(label=_("Address Line 2"), required=False)
    address_city = forms.CharField(label=_("City"))
    address_postCode = forms.CharField(label=_("Postal Code"))
    address_country = forms.ChoiceField(label=_("Country"), choices=RequiredCountries())

    def clean_dateOfBirth(self):
        dateOfBirth = self.cleaned_data["dateOfBirth"]
        return dateOfBirth.isoformat()


@register_bank_form(wise_type="chinese_alipay", currencies=["CNY"])
class AlipayDetailsForm(BankDetailsForm):
    description = _("Chinese Alipay Transfers")
    recipient_name = forms.CharField(label=_("Name"))
    alipayId = forms.CharField(
        label=_("Alipay ID"), help_text=_("Mobile number or email address")
    )
