from django import forms
from django.utils.translation import gettext_lazy as _

from crispy_forms.helper import FormHelper


class RequestSubmitForm(forms.Form):
    compliant = forms.BooleanField(
        label=_(
            "The expenses, for which I seek reimbursement, are compliant with "
            "the policies of the project and the fiscal sponsor."
        )
    )
    sole_source = forms.BooleanField(
        label=_(
            "I have not sought nor will seek reimbursement of these "
            "expenses from any other source."
        )
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
