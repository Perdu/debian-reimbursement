from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import FileResponse, HttpResponse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.module_loading import import_string
from django.utils.translation import gettext as _
from django.urls import reverse
from django.views.decorators.clickjacking import xframe_options_sameorigin
from django.views.generic.detail import DetailView

from image_wrangler.authentication import LoginOrAccessKeyRequiredMixin

from reimbursements.forms.request import RequestForm
from reimbursements.forms.requestline import RequestLineFormHelper, RequestLineFormset
from reimbursements.lifecycle import (
    request_created,
    request_currency_changed,
    request_payer_changed,
    request_updated,
)
from reimbursements.models import Request, RequestType
from reimbursements.views.generic import (
    DetailActionView,
    FormAndSetCreateView,
    FormAndSetUpdateView,
)


class RequestView(LoginOrAccessKeyRequiredMixin, DetailView):
    model = Request
    template_name = "reimbursements/request.html"
    context_object_name = "object"

    def get_context_data(self, **kwargs):
        access = self.object.user_access(
            self.request.user, self.request.valid_access_key
        )
        if not access:
            raise PermissionDenied()
        context = super().get_context_data(**kwargs)
        context["access"] = access

        statuses = []
        if self.object.requester:
            for path in settings.MEMBERSHIP_STATUSES:
                fn = import_string(path)
                statuses.append(fn(self.object.requester))
        context["membership_statuses"] = statuses

        context["groups"] = Group.objects.all()

        return context


@method_decorator(xframe_options_sameorigin, name="dispatch")
class RequestPDFBundleView(LoginRequiredMixin, DetailView):
    model = Request
    context_object_name = "object"

    def render_to_response(self, context):
        if not self.object.user_access(self.request.user):
            raise PermissionDenied()
        return FileResponse(self.object.pdf_bundle)


class RequestBeancountView(LoginRequiredMixin, DetailView):
    model = Request
    context_object_name = "object"

    def render_to_response(self, context):
        if not self.object.user_access(self.request.user):
            raise PermissionDenied()
        return HttpResponse(self.object.generate_beancount_report, content_type="text/plain")


class RequestCreateView(LoginRequiredMixin, FormAndSetCreateView):
    template_name = "reimbursements/edit_request.html"
    form_class = RequestForm
    formset_class = RequestLineFormset

    def get(self, request, *args, **kwargs):
        if not self.request.user.get_full_name():
            messages.add_message(
                self.request, messages.INFO, _("Please add your name to your account")
            )
            return redirect(reverse("profile-edit"))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["formset_helper"] = RequestLineFormHelper()
        context["expense_type_map"] = RequestType.expense_type_map()
        return context

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        form.request_user = self.request.user
        return form

    def forms_valid(self, form, formset):
        with transaction.atomic():
            r = super().forms_valid(form, formset)
            request_created(request=self.object, actor=self.request.user)
        return r


class RequestEditView(LoginRequiredMixin, FormAndSetUpdateView):
    template_name = "reimbursements/edit_request.html"
    model = Request
    context_object_name = "object"
    form_class = RequestForm
    formset_class = RequestLineFormset

    def get_context_data(self, **kwargs):
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only edit your own requests")
        context = super().get_context_data(**kwargs)
        context["formset_helper"] = RequestLineFormHelper()
        context["expense_type_map"] = RequestType.expense_type_map()
        return context

    def forms_valid(self, form, formset):
        with transaction.atomic():
            r = super().forms_valid(form, formset)
            request_updated(request=self.object, actor=self.request.user)
        return r


class RequestCurrencyChangeView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = request = self.get_object()

        access = request.user_access(self.request.user)
        if access.get("admin", False):
            pass
        elif request.state == Request.States.APPROVED:
            if not access.get("requester", False):
                raise PermissionDenied(
                    "Only the requester can change a request's currency, at this stage"
                )
        elif request.state == Request.States.PENDING_PAYMENT:
            if not access.get("payer", False):
                raise PermissionDenied(
                    "Only the assigned payer can change a request's currency, at this stage"
                )

        new_currency = self.request.POST.get("currency")
        if request.currency == new_currency:
            raise PermissionDenied(f"Request is already in {new_currency}")

        request_currency_changed(
            request=request,
            actor=self.request.user,
            details=self.request.POST.get("details"),
            new_currency=self.request.POST.get("currency"),
        )
        return redirect(request.get_absolute_url())


class RequestPayerChangeView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = request = self.get_object()

        if request.state in (Request.States.DELETED, Request.States.PAID):
            raise PermissionDenied("Completed requests can't change payer")

        access = request.user_access(self.request.user)
        if access.get("payer", False) or access.get("admin", False):
            pass
        elif access.get("approver", False):
            if request.state == Request.States.PENDING_PAYMENT:
                raise PermissionDenied("Already submitted for payment.")
        else:
            raise PermissionDenied("Only the approver or payer can reassign the payer.")

        new_payer_id = self.request.POST.get("payer")
        new_payer_group = Group.objects.get(id=new_payer_id)

        request_payer_changed(
            request=request,
            actor=self.request.user,
            details=self.request.POST.get("details"),
            new_payer=new_payer_group,
        )
        return redirect(request.get_absolute_url())
