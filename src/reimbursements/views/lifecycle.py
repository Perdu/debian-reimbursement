from decimal import Decimal
from typing import TYPE_CHECKING

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import HttpResponseNotAllowed
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView

from reimbursements.forms.reimbursement import RequestReimbursementForm
from reimbursements.forms.lifecycle import RequestSubmitForm
from reimbursements.lifecycle import (
    reimbursement_returned_for_more_info,
    request_approved,
    request_commented,
    request_draft_deleted,
    request_proposed,
    request_proposed_increase,
    request_reimbursed,
    request_rejected,
    request_returned_for_more_info,
    request_submitted,
    request_withdrawn,
)
from reimbursements.models import (
    ExpenseType,
    ReceiptLine,
    ReimbursementCost,
    ReimbursementLine,
    Request,
)
from reimbursements.views.generic import DetailActionView

if TYPE_CHECKING:
    from django.views.generic.edit import _FormT


class RequestSubmitDraftView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only edit your own requests")
        if self.object.state != Request.States.DRAFT:
            raise PermissionDenied("Only DRAFT requests can be submitted")
        if not self.request.user.get_full_name():
            raise PermissionDenied("Requesters need a name in their profile")
        details = request.POST.get("details")
        request_proposed(request=self.object, actor=self.request.user, details=details)
        return redirect(self.object.get_absolute_url())


class RequestCommentView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.user_access(self.request.user):
            raise PermissionDenied("Can only comment on requests you have access to")
        details = request.POST.get("details")
        request_commented(request=self.object, actor=self.request.user, details=details)
        return redirect(self.object.get_absolute_url())


class RequestDeleteView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only delete your own requests")
        details = request.POST.get("details")
        if self.object.state == Request.States.DRAFT:
            request_draft_deleted(
                request=self.object, actor=self.request.user, details=details
            )
        elif self.object.state == Request.States.APPROVED:
            request_withdrawn(
                request=self.object, actor=self.request.user, details=details
            )
        else:
            raise PermissionDenied("Only DRAFT and APPROVED requests can be deleted")
        return redirect(self.object.get_absolute_url())


class RequestSubmitIncreaseView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only edit your own requests")
        if self.object.state != Request.States.APPROVED:
            raise PermissionDenied(
                "Only APPROVED requests can be submitted for an increase"
            )
        details = request.POST.get("details")
        request_proposed_increase(
            request=self.object, actor=self.request.user, details=details
        )
        return redirect(reverse("request-edit", kwargs=self.kwargs))


class RequestSubmitView(LoginRequiredMixin, FormView):
    template_name = "reimbursements/submit_request.html"
    form_class = RequestSubmitForm

    def get_form(self, form_class=None):
        self.object = Request.objects.get(pk=self.kwargs["pk"])
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only edit your own requests")
        if self.object.state != Request.States.APPROVED:
            raise PermissionDenied("Only APPROVED requests can be submitted")
        return super().get_form(form_class=form_class)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object"] = self.object
        return context

    def form_valid(self, form: "_FormT"):
        request_submitted(request=self.object, actor=self.request.user)
        return redirect(self.object.get_absolute_url())


class RequestReviewView(LoginRequiredMixin, DetailView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        access = self.object.user_access(self.request.user)
        if not (access.get("approver", False) or access.get("admin", False)):
            raise PermissionDenied(
                "Can only review requests that you are an approver for"
            )
        if self.object.state != Request.States.PENDING_APPROVAL:
            raise PermissionDenied("Only PENDING_APPROVAL requests can be reviewed")
        details = request.POST.get("details")
        if request.POST.get("approve"):
            request_approved(
                request=self.object, actor=self.request.user, details=details
            )
        elif request.POST.get("reject"):
            request_rejected(
                request=self.object, actor=self.request.user, details=details
            )
        elif request.POST.get("more_info"):
            request_returned_for_more_info(
                request=self.object, actor=self.request.user, details=details
            )
        else:
            raise Exception("Unknown action")
        return redirect(self.object.get_absolute_url())

    def get(self, request, *args, **kwargs):
        # DetailView provides a get()
        return HttpResponseNotAllowed(["post"])


class RequestReimburseView(LoginRequiredMixin, FormView):
    template_name = "reimbursements/reimburse_request.html"
    form_class = RequestReimbursementForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.object = Request.objects.get(pk=self.kwargs["pk"])
        kwargs["request"] = self.object
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not self.object.user_access(self.request.user).get("payer", False):
            raise PermissionDenied("Only the assigned payer can edit your requests")
        if self.object.state != Request.States.PENDING_PAYMENT:
            raise PermissionDenied("Only PENDING_PAYMENT requests can be reimbursed")
        context["object"] = self.object
        context["line_outside_budget"] = any(
            True
            for line in self.object.summarize_by_type().values()
            if line.spent > Decimal(0)
            and line.requested == Decimal(0)
            and not line.special
        )
        context["line_outside_conversion_limits"] = any(
            True
            for line in ReceiptLine.objects.filter(receipt__request=self.object)
            if not line.conversion_within_limits
        )
        return context

    def form_valid(self, form):
        with transaction.atomic():
            data = form.cleaned_data.copy()
            details = data.pop("details")
            fee = data.pop("fee")
            fee_currency = data.pop("fee_currency")
            fee_details = data.pop("fee_details")
            for k, v in data.items():
                ReimbursementLine(
                    request=self.object,
                    expense_type=ExpenseType.objects.get(id=int(k)),
                    amount=v,
                ).save()
            if fee:
                ReimbursementCost(
                    request=self.object,
                    description=fee_details,
                    amount=fee,
                    currency=fee_currency,
                ).save()
            request_reimbursed(self.object, self.request.user, details=details)
        return redirect(self.object.get_absolute_url())


class RequestReimburseReturnView(LoginRequiredMixin, DetailActionView):
    model = Request
    context_object_name = "object"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        access = self.object.user_access(self.request.user)
        if not (access.get("payer", False) or access.get("admin", False)):
            raise PermissionDenied("Only the assigned payer can edit your requests")
        if self.object.state != Request.States.PENDING_PAYMENT:
            raise PermissionDenied(
                "Only PENDING_PAYMENT requests can be returned from reimbursement"
            )
        details = request.POST.get("details")
        reimbursement_returned_for_more_info(
            request=self.object, actor=self.request.user, details=details
        )
        return redirect(reverse("request", kwargs=self.kwargs))
