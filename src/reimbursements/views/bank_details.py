from typing import TYPE_CHECKING

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView

from reimbursements.forms.bank_details import select_form
from reimbursements.models import Profile, Request

if TYPE_CHECKING:
    from django.views.generic.edit import _FormT


class BankFormSelectorView(LoginRequiredMixin, DetailView):
    model = Request
    template_name = "reimbursements/bank_form_selector.html"
    context_object_name = "object"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.object.bank_details:
            wise_type = self.object.bank_details.get("wise_type")
            context["selected"] = select_form(wise_type=wise_type)
        else:
            try:
                bank_details = self.request.user.profile.bank_details
                if wise_type := bank_details.get("wise_type"):
                    context["saved"] = select_form(wise_type=wise_type)
            except Profile.DoesNotExist:
                pass
        if recommended := select_form(currency=self.object.currency):
            context["recommended"] = recommended
        context["defaults"] = [
            select_form(wise_type="swift"),
            select_form(wise_type="wise"),
        ]
        return context


class BankDetailsView(LoginRequiredMixin, UpdateView):
    model = Request
    template_name = "reimbursements/bank_details.html"
    context_object_name = "object"

    def get_form_class(self):
        wise_type = self.kwargs["wise_type"]
        return select_form(wise_type=wise_type)

    def get_initial(self):
        try:
            bank_details = self.request.user.profile.bank_details
        except Profile.DoesNotExist:
            bank_details = None
        if not bank_details:
            bank_details = {
                "recipient_name": self.request.user.get_full_name(),
                "email": self.request.user.email,
            }
        return bank_details

    def get_context_data(self, **kwargs):
        if not self.object.user_access(self.request.user).get("requester", False):
            raise PermissionDenied("Can only edit your own requests")
        context = super().get_context_data(**kwargs)
        return context

    def form_valid(self, form: "_FormT"):
        Profile.objects.update_or_create(
            user=self.request.user, defaults={"bank_details": form.cleaned_data}
        )
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.get_absolute_url()
