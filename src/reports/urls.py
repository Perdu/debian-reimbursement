from django.urls import path

from reports.views import ApproverReport, PayerReport

urlpatterns = [
    path("approver/<int:pk>", ApproverReport.as_view(), name="report-approver"),
    path("payer/<int:pk>", PayerReport.as_view(), name="report-payer"),
]
