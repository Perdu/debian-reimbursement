from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.views.generic.detail import DetailView

from reimbursements.models import Request


class RequestsReport(DetailView):

    template_name = "reports/requests.html"
    model = Group

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        if not self.request.user.groups.filter(pk=obj.pk).exists():
            raise PermissionDenied("Not a member of this group")
        return obj

    def get_requests_queryset(self):
        raise NotImplementedError("Implemented in subclasses")

    def get_requests(self):
        return (
            self.get_requests_queryset()
            .exclude(state=Request.States.DELETED)
            .exclude(visible=False)
        )

    def get_context_data(self, **kwargs):
        context = {
            "requests": self.get_requests(),
        }
        context.update(kwargs)
        return super().get_context_data(**context)


class ApproverReport(RequestsReport):

    def get_requests_queryset(self):
        return self.object.requests_approver


class PayerReport(RequestsReport):

    def get_requests_queryset(self):
        return self.object.requests_payer
