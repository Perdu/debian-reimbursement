as discussed during the DebCamp meeting yesterday:

(version 09. June 2020, yes not a typo, we are that slow :()

# Scope
* Request of Reimbursements
* Budget Lines (global or per-requester budget or just multiply the budget lines)
* Approval process based on budget line (small workflow, 2-3 steps: e.g. Debconf: Requester-Debconf Team-DPL)
* State should be communicated via email
* End result: Todo List for each stake-holder and bookable financial reports (e.g. ledger files...)
* Optional: Integration of Transferwise API
* Stakeholders: Requester, Approver Groups (assigned to budget line and to TO)
* Payout Information configured on the Requester at an early step
* Authentication: OpenID connect login (salsa), check if debian group.. or sync from somewhere (NM API)
* Currency conversion rates are handled for the budget side; user enters line items incl. conversion (e.g. cost after credit card charges) in his home currency
* Only handle currencies which at least one TO can pay in (~18 currencies at TW)

Exclusions:
* No full Workflow Engine
* One instance per organisation (no multi-client support)
* Only support for similar workflow
* One or two approver steps per request (empty second group = auto-approved to next step)

# Walk-through 1
Standard Case: Reimbursement Travel via debian.ch
* Admin defines budget line for general Debian travel reimbursement ("Debian Travel (permanent)","Debconf20 travel (valid until 20.02.2021)" all DDs eligible, e.g. 50k, requester-groups allow all DDs, Approver DPL, Expiration date, allowed TOs)
* Requester logs into System
* Requester chooses "New expense request"
* Requester chooses  "Budget line (named differently for users ~ project / whatever)"
* Requester chooses TO out of available choices for the budget line
* Requester enters maximum amount, one of the available currencies and expense request description and estimated date
* System converts the value to the budget line value, checks amount availability and blocks the amount tentatively
* Requestor gets a confirmation screen and can log out
* System adds expense request to work items list of approver group (of the budget line)
* Approver logs into the system (Budget line approver = DPL in this case)
* Is shown list of pending expense request (also sees payment requests in a separate list if they have such a role), clicks on one of them, gets details
* Actions: approve, deny, request more info (free text)
* Requester gets email with status update (needs to login if moreinfo)
* Approver can log out
* Requester spends money
* Requester logs in again and chooses "Payment Request"
* Chooses from approved "expense requests"
* Requester is asked for bank details and payout currency? (possible to store)
* Requester enters line items, each with description, date of expense, original amount+currency and amount in payout currency (including fees), uploads pdf and assigns it to line item or chooses previously uploaded document (allow pdf,png and jpg)
* Checkbox if further payment requests are necessary for this expense request (e.g. flights paid, hotel later)
* Requester sends payment request and logs out
* DPL logs in (group DPL member)
* DPL approved payment request (same options as next step)
* DPL logs out
* Payment request is added to work item list of TO treasurer
* TO treasurer approves, rejects or moreinfo's request (info to requester, reject cancels payment request and it needs to be entered again)
* TO treasurer chooses payments requests to process
* System generates item for payment processing and ledger line (user can mark multiple processes for this function) or issues the corresponding API requests
* TO treasurer pays and confirms  (multiple)
* System sends email to requester and closes payment request (and if the tickbox was not marked the matching expense request)
* TO treasurer logs out

# Walkthrough 2
DebConf Case: Reimbursement of bursary request via SPI
* Admin defines budget line for DebConfxx reimbursement (valid until 20.02.20xx+1)" Debconf Bursary Team is eligible to submit requests (for others), DPL approves this budget line (via signed email as per the normal DebConf budget approval process)
* Expense requests are batch imported from approved bursary grants (wafer csv file) -> user matching via email possession proof
  (claim approved expense request link to email)
* User logs in
* User creates payment request as above
* User logs out
* Approval + payment workflow as above (Approval by DebConf bursary team, Approval + Payout by TO treasurer)

# Walkthrough 3
Personal allowance case
* 01.01.xx create "Personal discretionary budget / allowance 20xx" budget line
* Send every DD a pre-approved expense request via email, value tbd. EUR
* DD logs in and creates 1 .. n payment requests against his personal expense request. DD logs out.

Please feel free to ask questions and discuss in #debian-treasurer (@stefano, peb: please join the channel)

Kind regards,
Daniel

